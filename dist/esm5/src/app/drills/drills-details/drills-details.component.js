/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PracticePlannerService } from '../practice-planner.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/filter';
var DrillsDetailsComponent = /** @class */ (function () {
    function DrillsDetailsComponent(practiceService, route, router, datePipe, sanitizer, ngxSmartModalService) {
        this.practiceService = practiceService;
        this.route = route;
        this.router = router;
        this.datePipe = datePipe;
        this.sanitizer = sanitizer;
        this.ngxSmartModalService = ngxSmartModalService;
    }
    /**
     * @return {?}
     */
    DrillsDetailsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var drillIndex;
        this.route.queryParams
            .filter((/**
         * @param {?} params
         * @return {?}
         */
        function (params) { return params.id; }))
            .subscribe((/**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            drillIndex = params.id;
        }));
        this.practiceService.getRequest("api/practicePlans/" + drillIndex).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.drill = res;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.log('errr', error1);
        }));
        this.practiceService.getRequest('api/saveplans').subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.plans = res;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.log('error', error1);
        }));
    };
    /**
     * @return {?}
     */
    DrillsDetailsComponent.prototype.back = /**
     * @return {?}
     */
    function () {
        this.router.navigate(['drills']);
    };
    /**
     * @param {?} index
     * @return {?}
     */
    DrillsDetailsComponent.prototype.remove = /**
     * @param {?} index
     * @return {?}
     */
    function (index) {
        this.ngxSmartModalService.getModal('removeModal').open();
        this.selectedIndex = index;
    };
    /**
     * @return {?}
     */
    DrillsDetailsComponent.prototype.submitRemove = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.drill.items.splice(this.selectedIndex, 1);
        this.practiceService.putRequest("api/practicePlans/" + this.drill.id, this.drill).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.ngxSmartModalService.getModal('removeModal').close();
            _this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.log('remove error', error1);
        }));
    };
    /**
     * @param {?} drill
     * @param {?} i
     * @return {?}
     */
    DrillsDetailsComponent.prototype.edit = /**
     * @param {?} drill
     * @param {?} i
     * @return {?}
     */
    function (drill, i) {
        this.selectedDrill = drill;
        this.selectedIndex = i;
        this.ngxSmartModalService.getModal('editModal').open();
    };
    /**
     * @return {?}
     */
    DrillsDetailsComponent.prototype.submitEdit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.selectedIndex || !isNaN(this.selectedIndex)) {
            this.drill.items[this.selectedIndex] = this.selectedDrill;
        }
        else {
            this.drill.items.push(this.selectedDrill);
        }
        this.practiceService.putRequest("api/practicePlans/" + this.drill.id, this.drill).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.ngxSmartModalService.getModal('editModal').close();
            _this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.error('edit error', error1);
        }));
    };
    /**
     * @return {?}
     */
    DrillsDetailsComponent.prototype.create = /**
     * @return {?}
     */
    function () {
        this.selectedDrill = {
            title: '',
            descriptionA: '',
            descriptionB: '',
            descriptionC: '',
            descriptionD: '',
            descriptionE: '',
            descriptionF: '',
            videoURL: '',
            id: this.datePipe.transform(new Date(), 'yyyy MM dd HH MM SS MM').replace(/\s/g, '')
        };
        this.ngxSmartModalService.getModal('editModal').open();
    };
    /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    DrillsDetailsComponent.prototype.check = /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    function (plan, drill) {
        /** @type {?} */
        var checked = plan.description[0].find((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            return item.id === drill.id;
        }));
        return checked ? true : false;
    };
    /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    DrillsDetailsComponent.prototype.addToPlan = /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    function (plan, drill) {
        /** @type {?} */
        var reomveIndex;
        /** @type {?} */
        var checked = plan.description[0].find((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        function (item, index) {
            if (item.id === drill.id) {
                reomveIndex = index;
            }
            return item.id === drill.id;
        }));
        if (checked) {
            plan.description[0].splice(reomveIndex, 1);
            console.log('remove', plan);
        }
        else {
            plan.description[0].push(drill);
            console.log('add', plan);
        }
        this.practiceService.putRequest("api/saveplans/" + plan.id, plan).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            console.log('added', res);
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.error('err', error1);
        }));
    };
    DrillsDetailsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-drills-details',
                    template: "<div *ngIf=\"drill\" class=\"\">\n  <div>\n    <div class=\"back\">\n      <button (click)=\"back()\" type=\"button\" class=\"btn btn-primary\">\n        <img src=\"../../../assets/images/60577.png\" alt=\"\">\n        Back\n      </button>\n      <button (click)=\"create()\" class=\"btn btn-main\">Create Drill</button>\n    </div>\n  </div>\n  <h2 class=\"text-center main-title\">{{drill.title}}</h2>\n  <div *ngFor=\"let item of drill.items; let i = index\" class=\"drill row\">\n      <div class=\"drill-head-section flexbox\">\n        <h3 class=\"drill-heading m-0\">{{item.title}}</h3>\n        <div class=\"actions\">\n          <span class=\"glyphicon glyphicon-pencil btn\" title=\"Edit\" (click)=\"edit(item, i)\"></span>\n          <span class=\"glyphicon glyphicon-trash btn\" title=\"Remove\" (click)=\"remove(i)\"></span>\n          <span class=\"dropdown\" dropdown>\n            <button class=\"btn btn-link\" dropdown-open>Add to plan</button>\n            <ul class=\"dropdown-menu\" dropdown-not-closable-zone>\n              <li *ngFor=\"let plan of plans\">\n                <input type=\"checkbox\" [checked]=\"check(plan, item)\" (change)=\"addToPlan(plan, item)\">\n                <label>\n                  {{plan.description[1].name}}\n                </label>\n              </li>\n            </ul>\n          </span>\n        </div>\n      </div>\n      <div class=\"drill-content\">\n        <p *ngIf=\"item.descriptionA\">{{item.descriptionA}}</p>\n        <p *ngIf=\"item.descriptionB\">{{item.descriptionB}}</p>\n        <p *ngIf=\"item.descriptionC\">{{item.descriptionC}}</p>\n        <p *ngIf=\"item.descriptionD\">{{item.descriptionD}}</p>\n        <p *ngIf=\"item.descriptionE\">{{item.descriptionE}}</p>\n        <p *ngIf=\"item.descriptionF\">{{item.descriptionF}}</p>\n        <p class=\"mb-0\">\n          <iframe width=\"520\" height=\"310\" class=\"e2e-iframe-trusted-src\" [src]=\"sanitizer.bypassSecurityTrustResourceUrl(item.videoURL)\" frameborder=\"0\" allowfullscreen></iframe>\n        </p>\n      </div>\n  </div>\n</div>\n<ngx-smart-modal  *ngIf=\"drill\" #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal  *ngIf=\"drill\" #editModal identifier=\"editModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Edit Drill</p>\n   <div *ngIf=\"selectedDrill\">\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Title</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.title\" name=\"\" id=\"title\" cols=\"30\" rows=\"3\">{{selectedDrill.title}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description A</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionA\" name=\"\" id=\"descriptionA\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionA}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description B</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionB\" name=\"\" id=\"descriptionB\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionB}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description C</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionC\" name=\"\" id=\"descriptionC\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionC}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description D</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionD\" name=\"\" id=\"descriptionD\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionD}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description E</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionE\" name=\"\" id=\"descriptionE\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionE}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description F</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionF\" name=\"\" id=\"descriptionF\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionF}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Video Url</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.videoURL\" name=\"\" id=\"videoURL\" cols=\"30\" rows=\"3\">{{selectedDrill.videoURL}}</textarea>\n         </div>\n       </div>\n   </div>\n\n  <div class=\"text-center\">\n    <button class=\"btn btn-danger\" (click)=\"editModal.close()\">Cancel</button>\n    <button class=\"btn btn-primary\" (click)=\"submitEdit()\">Save</button>\n  </div>\n  </div>\n</ngx-smart-modal>\n",
                    styles: [".drill{margin:25px auto;background:#fff}.drill .actions span{display:inline-block;margin-left:10px;padding:0}.actions .btn:hover{color:#fff}.actions .btn:active{box-shadow:none}.actions .btn:focus{color:#fff;outline:0}.btn-link{color:#fff;text-decoration:underline}.back img{height:17px}.back .btn{margin-right:10px}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}.modal-content-inner .form-control{resize:vertical}.main-title{color:#035596}.glyphicon{top:0}.dropdown-menu{top:40px;right:0;left:auto;padding:10px 5px;margin:5px 0}.dropdown-menu li{display:flex;align-items:center;margin-bottom:5px}.dropdown-menu li input{margin:0 5px 0 0}.dropdown-menu li label{margin-bottom:0;color:#000;font-weight:500}.input-group-addon{width:110px}.btn-primary{font-weight:600;text-transform:uppercase;border-color:transparent;background-color:#0070c9}.btn-primary:hover{background-color:#035596}.drill-head-section{padding:15px;background:#0070c9;color:#fff;border-top-left-radius:4px;border-top-right-radius:4px;justify-content:space-between}.drill-heading{color:#fff;font-size:18px;line-height:unset;padding:4px 0}.drill-content{padding:10px 20px 30px}.mb-0{margin-bottom:0}iframe{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    DrillsDetailsComponent.ctorParameters = function () { return [
        { type: PracticePlannerService },
        { type: ActivatedRoute },
        { type: Router },
        { type: DatePipe },
        { type: DomSanitizer },
        { type: NgxSmartModalService }
    ]; };
    DrillsDetailsComponent.propDecorators = {
        data: [{ type: Input }]
    };
    return DrillsDetailsComponent;
}());
export { DrillsDetailsComponent };
if (false) {
    /** @type {?} */
    DrillsDetailsComponent.prototype.data;
    /** @type {?} */
    DrillsDetailsComponent.prototype.drill;
    /** @type {?} */
    DrillsDetailsComponent.prototype.plans;
    /** @type {?} */
    DrillsDetailsComponent.prototype.selectedDrill;
    /** @type {?} */
    DrillsDetailsComponent.prototype.selectedIndex;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.practiceService;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.datePipe;
    /** @type {?} */
    DrillsDetailsComponent.prototype.sanitizer;
    /** @type {?} */
    DrillsDetailsComponent.prototype.ngxSmartModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJpbGxzLWRldGFpbHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZHJpbGwvIiwic291cmNlcyI6WyJzcmMvYXBwL2RyaWxscy9kcmlsbHMtZGV0YWlscy9kcmlsbHMtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLDBCQUEwQixDQUFDO0FBRWxDO0lBYUUsZ0NBQ1UsZUFBdUMsRUFDdkMsS0FBcUIsRUFDckIsTUFBYyxFQUNkLFFBQWtCLEVBQ25CLFNBQXVCLEVBQ3ZCLG9CQUEwQztRQUx6QyxvQkFBZSxHQUFmLGVBQWUsQ0FBd0I7UUFDdkMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO0lBQy9DLENBQUM7Ozs7SUFFTCx5Q0FBUTs7O0lBQVI7UUFBQSxpQkF5QkM7O1lBeEJLLFVBQVU7UUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVc7YUFDbkIsTUFBTTs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLEVBQUUsRUFBVCxDQUFTLEVBQUM7YUFDM0IsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUNmLFVBQVUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsdUJBQXFCLFVBQVksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDMUUsVUFBQyxHQUFRO1lBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDbkIsQ0FBQzs7OztRQUNELFVBQUEsTUFBTTtZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUMsRUFDRixDQUFDO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUzs7OztRQUN4RCxVQUFDLEdBQVE7WUFDUCxLQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNuQixDQUFDOzs7O1FBQ0QsVUFBQSxNQUFNO1lBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNGLENBQUM7SUFFTCxDQUFDOzs7O0lBRUQscUNBQUk7OztJQUFKO1FBQ0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsdUNBQU07Ozs7SUFBTixVQUFPLEtBQUs7UUFDVixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCw2Q0FBWTs7O0lBQVo7UUFBQSxpQkFXQztRQVZDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLHVCQUFxQixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUzs7OztRQUN6RixVQUFDLEdBQVE7WUFDUCxLQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzFELEtBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDO1FBQzNCLENBQUM7Ozs7UUFDRCxVQUFBLE1BQU07WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN0QyxDQUFDLEVBQ0YsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVELHFDQUFJOzs7OztJQUFKLFVBQUssS0FBSyxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCwyQ0FBVTs7O0lBQVY7UUFBQSxpQkFlQztRQWRDLElBQUssSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDckQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7U0FDM0Q7YUFBTTtZQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyx1QkFBcUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDekYsVUFBQyxHQUFRO1lBQ1AsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN4RCxLQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQztRQUMzQixDQUFDOzs7O1FBQ0QsVUFBQSxNQUFNO1lBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsdUNBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLGFBQWEsR0FBRztZQUNuQixLQUFLLEVBQUUsRUFBRTtZQUNULFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFlBQVksRUFBRSxFQUFFO1lBQ2hCLFFBQVEsRUFBRSxFQUFFO1lBQ1osRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUMsd0JBQXdCLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQztTQUNuRixDQUFDO1FBQ0YsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN6RCxDQUFDOzs7Ozs7SUFFRCxzQ0FBSzs7Ozs7SUFBTCxVQUFNLElBQUksRUFBRSxLQUFLOztZQUNYLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBRSxVQUFDLElBQUk7WUFDM0MsT0FBTyxJQUFJLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxFQUFFLENBQUM7UUFDOUIsQ0FBQyxFQUFFO1FBQ0gsT0FBTyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVELDBDQUFTOzs7OztJQUFULFVBQVUsSUFBSSxFQUFFLEtBQUs7O1lBQ2YsV0FBVzs7WUFDWCxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJOzs7OztRQUFFLFVBQUMsSUFBSSxFQUFFLEtBQUs7WUFDaEQsSUFBSyxJQUFJLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pCLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDckI7WUFDSCxPQUFPLElBQUksQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUM5QixDQUFDLEVBQUM7UUFDRixJQUFLLE9BQU8sRUFBRztZQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM3QjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDMUI7UUFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxtQkFBaUIsSUFBSSxDQUFDLEVBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O1FBQ3pFLFVBQUMsR0FBUTtZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLENBQUM7Ozs7UUFDRCxVQUFBLE1BQU07WUFDSixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ0YsQ0FBQztJQUNKLENBQUM7O2dCQTNJSixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsNm9MQUE4Qzs7aUJBRS9DOzs7O2dCQVZRLHNCQUFzQjtnQkFEdEIsY0FBYztnQkFBRSxNQUFNO2dCQUl0QixRQUFRO2dCQUZSLFlBQVk7Z0JBQ1osb0JBQW9COzs7dUJBVzFCLEtBQUs7O0lBcUlSLDZCQUFDO0NBQUEsQUE1SUQsSUE0SUM7U0F2SVksc0JBQXNCOzs7SUFFakMsc0NBQW1COztJQUNuQix1Q0FBa0I7O0lBQ2xCLHVDQUFrQjs7SUFDbEIsK0NBQTBCOztJQUMxQiwrQ0FBNkI7Ozs7O0lBRzNCLGlEQUErQzs7Ozs7SUFDL0MsdUNBQTZCOzs7OztJQUM3Qix3Q0FBc0I7Ozs7O0lBQ3RCLDBDQUEwQjs7SUFDMUIsMkNBQThCOztJQUM5QixzREFBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFByYWN0aWNlUGxhbm5lclNlcnZpY2UgfSBmcm9tICcuLi9wcmFjdGljZS1wbGFubmVyLnNlcnZpY2UnO1xuaW1wb3J0IHsgRG9tU2FuaXRpemVyIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsU2VydmljZSB9IGZyb20gJ25neC1zbWFydC1tb2RhbCc7XG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2ZpbHRlcic7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1kcmlsbHMtZGV0YWlscycsXG4gIHRlbXBsYXRlVXJsOiAnLi9kcmlsbHMtZGV0YWlscy5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2RyaWxscy1kZXRhaWxzLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEcmlsbHNEZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBkYXRhOiBhbnk7XG4gIHB1YmxpYyBkcmlsbDogYW55O1xuICBwdWJsaWMgcGxhbnM6IGFueTtcbiAgcHVibGljIHNlbGVjdGVkRHJpbGw6IGFueTtcbiAgcHVibGljIHNlbGVjdGVkSW5kZXg6IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHByYWN0aWNlU2VydmljZTogUHJhY3RpY2VQbGFubmVyU2VydmljZSxcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgZGF0ZVBpcGU6IERhdGVQaXBlLFxuICAgIHB1YmxpYyBzYW5pdGl6ZXI6IERvbVNhbml0aXplcixcbiAgICBwdWJsaWMgbmd4U21hcnRNb2RhbFNlcnZpY2U6IE5neFNtYXJ0TW9kYWxTZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgbGV0IGRyaWxsSW5kZXg7XG4gICAgdGhpcy5yb3V0ZS5xdWVyeVBhcmFtc1xuICAgICAgLmZpbHRlcihwYXJhbXMgPT4gcGFyYW1zLmlkKVxuICAgICAgLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgICAgICBkcmlsbEluZGV4ID0gcGFyYW1zLmlkO1xuICAgICAgfSk7XG4gICAgdGhpcy5wcmFjdGljZVNlcnZpY2UuZ2V0UmVxdWVzdChgYXBpL3ByYWN0aWNlUGxhbnMvJHtkcmlsbEluZGV4fWApLnN1YnNjcmliZShcbiAgICAgIChyZXM6IGFueSkgPT4ge1xuICAgICAgICB0aGlzLmRyaWxsID0gcmVzO1xuICAgICAgfSxcbiAgICAgIGVycm9yMSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdlcnJyJywgZXJyb3IxKTtcbiAgICAgIH1cbiAgICApO1xuXG4gICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLmdldFJlcXVlc3QoJ2FwaS9zYXZlcGxhbnMnKS5zdWJzY3JpYmUoXG4gICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgICB0aGlzLnBsYW5zID0gcmVzO1xuICAgICAgIH0sXG4gICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgIGNvbnNvbGUubG9nKCdlcnJvcicsIGVycm9yMSk7XG4gICAgICAgfVxuICAgICApO1xuXG4gIH1cblxuICBiYWNrKCk6IHZvaWQge1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnZHJpbGxzJ10pO1xuICB9XG5cbiAgcmVtb3ZlKGluZGV4KTogdm9pZCB7XG4gICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgncmVtb3ZlTW9kYWwnKS5vcGVuKCk7XG4gICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gaW5kZXg7XG4gIH1cblxuICBzdWJtaXRSZW1vdmUoKTogdm9pZCB7XG4gICAgdGhpcy5kcmlsbC5pdGVtcy5zcGxpY2UodGhpcy5zZWxlY3RlZEluZGV4LCAxKTtcbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5wdXRSZXF1ZXN0KGBhcGkvcHJhY3RpY2VQbGFucy8ke3RoaXMuZHJpbGwuaWR9YCwgdGhpcy5kcmlsbCkuc3Vic2NyaWJlKFxuICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ3JlbW92ZU1vZGFsJykuY2xvc2UoKTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gTmFOO1xuICAgICAgfSxcbiAgICAgIGVycm9yMSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdyZW1vdmUgZXJyb3InLCBlcnJvcjEpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBlZGl0KGRyaWxsLCBpKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3RlZERyaWxsID0gZHJpbGw7XG4gICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gaTtcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdlZGl0TW9kYWwnKS5vcGVuKCk7XG4gIH1cblxuICBzdWJtaXRFZGl0KCk6IHZvaWQge1xuICAgIGlmICggdGhpcy5zZWxlY3RlZEluZGV4IHx8ICFpc05hTih0aGlzLnNlbGVjdGVkSW5kZXgpKSB7XG4gICAgICB0aGlzLmRyaWxsLml0ZW1zW3RoaXMuc2VsZWN0ZWRJbmRleF0gPSB0aGlzLnNlbGVjdGVkRHJpbGw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZHJpbGwuaXRlbXMucHVzaCh0aGlzLnNlbGVjdGVkRHJpbGwpO1xuICAgIH1cbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5wdXRSZXF1ZXN0KGBhcGkvcHJhY3RpY2VQbGFucy8ke3RoaXMuZHJpbGwuaWR9YCwgdGhpcy5kcmlsbCkuc3Vic2NyaWJlKFxuICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ2VkaXRNb2RhbCcpLmNsb3NlKCk7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRJbmRleCA9IE5hTjtcbiAgICAgIH0sXG4gICAgICBlcnJvcjEgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdlZGl0IGVycm9yJywgZXJyb3IxKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgY3JlYXRlKCk6IHZvaWQge1xuICAgIHRoaXMuc2VsZWN0ZWREcmlsbCA9IHtcbiAgICAgIHRpdGxlOiAnJyxcbiAgICAgIGRlc2NyaXB0aW9uQTogJycsXG4gICAgICBkZXNjcmlwdGlvbkI6ICcnLFxuICAgICAgZGVzY3JpcHRpb25DOiAnJyxcbiAgICAgIGRlc2NyaXB0aW9uRDogJycsXG4gICAgICBkZXNjcmlwdGlvbkU6ICcnLFxuICAgICAgZGVzY3JpcHRpb25GOiAnJyxcbiAgICAgIHZpZGVvVVJMOiAnJyxcbiAgICAgIGlkOiB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCd5eXl5IE1NIGRkIEhIIE1NIFNTIE1NJykucmVwbGFjZSgvXFxzL2csJycpXG4gICAgfTtcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdlZGl0TW9kYWwnKS5vcGVuKCk7XG4gIH1cblxuICBjaGVjayhwbGFuLCBkcmlsbCk6IGJvb2xlYW4ge1xuICAgIGxldCBjaGVja2VkID0gcGxhbi5kZXNjcmlwdGlvblswXS5maW5kKCAoaXRlbSkgPT4ge1xuICAgICAgcmV0dXJuIGl0ZW0uaWQgPT09IGRyaWxsLmlkO1xuICAgIH0gKTtcbiAgICByZXR1cm4gY2hlY2tlZCA/IHRydWUgOiBmYWxzZTtcbiAgfVxuXG4gIGFkZFRvUGxhbihwbGFuLCBkcmlsbCkge1xuICAgIGxldCByZW9tdmVJbmRleDtcbiAgICBsZXQgY2hlY2tlZCA9IHBsYW4uZGVzY3JpcHRpb25bMF0uZmluZCggKGl0ZW0sIGluZGV4KSA9PiB7XG4gICAgICAgIGlmICggaXRlbS5pZCA9PT0gZHJpbGwuaWQpIHtcbiAgICAgICAgICByZW9tdmVJbmRleCA9IGluZGV4O1xuICAgICAgICB9XG4gICAgICByZXR1cm4gaXRlbS5pZCA9PT0gZHJpbGwuaWQ7XG4gICAgfSk7XG4gICAgaWYgKCBjaGVja2VkICkge1xuICAgICAgcGxhbi5kZXNjcmlwdGlvblswXS5zcGxpY2UocmVvbXZlSW5kZXgsIDEpO1xuICAgICAgY29uc29sZS5sb2coJ3JlbW92ZScsIHBsYW4pO1xuICAgIH0gZWxzZSB7XG4gICAgICBwbGFuLmRlc2NyaXB0aW9uWzBdLnB1c2goZHJpbGwpO1xuICAgICAgY29uc29sZS5sb2coJ2FkZCcsIHBsYW4pO1xuICAgIH1cbiAgICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLnB1dFJlcXVlc3QoYGFwaS9zYXZlcGxhbnMvJHtwbGFuLmlkfWAsIHBsYW4pLnN1YnNjcmliZShcbiAgICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2FkZGVkJywgcmVzKTtcbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgICBjb25zb2xlLmVycm9yKCdlcnInLCBlcnJvcjEpO1xuICAgICAgICB9XG4gICAgICApO1xuICAgIH1cbn1cbiJdfQ==