/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @type {?} */
var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
var PracticePlannerService = /** @class */ (function () {
    function PracticePlannerService(http) {
        this.http = http;
    }
    /**
     * @param {?} url
     * @return {?}
     */
    PracticePlannerService.prototype.getRequest = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return this.http.get("" + url, { headers: headers });
    };
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    PracticePlannerService.prototype.postRequest = /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    function (url, data) {
        return this.http.post("" + url, data, { headers: headers });
    };
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    PracticePlannerService.prototype.putRequest = /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    function (url, data) {
        return this.http.put("" + url, data, { headers: headers });
    };
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    PracticePlannerService.prototype.patchRequest = /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    function (url, data) {
        return this.http.patch("" + url, data, { headers: headers });
    };
    /**
     * @param {?} url
     * @return {?}
     */
    PracticePlannerService.prototype.removeRequest = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return this.http.delete("" + url, { headers: headers });
    };
    PracticePlannerService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PracticePlannerService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ PracticePlannerService.ngInjectableDef = i0.defineInjectable({ factory: function PracticePlannerService_Factory() { return new PracticePlannerService(i0.inject(i1.HttpClient)); }, token: PracticePlannerService, providedIn: "root" });
    return PracticePlannerService;
}());
export { PracticePlannerService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PracticePlannerService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJhY3RpY2UtcGxhbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZHJpbGwvIiwic291cmNlcyI6WyJzcmMvYXBwL2RyaWxscy9wcmFjdGljZS1wbGFubmVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMvRCxPQUFPLHVCQUF1QixDQUFDOzs7O0lBRXpCLE9BQU8sR0FBRyxJQUFJLFdBQVcsRUFBRTtBQUNqQyxPQUFPLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0FBRW5EO0lBS0UsZ0NBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFBSSxDQUFDOzs7OztJQUV6QywyQ0FBVTs7OztJQUFWLFVBQVcsR0FBRztRQUNaLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBRyxHQUFLLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7Ozs7SUFFRCw0Q0FBVzs7Ozs7SUFBWCxVQUFZLEdBQUcsRUFBRSxJQUFJO1FBQ25CLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBRyxHQUFLLEVBQUUsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7O0lBRUQsMkNBQVU7Ozs7O0lBQVYsVUFBVyxHQUFHLEVBQUUsSUFBSTtRQUNsQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUcsR0FBSyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQUVELDZDQUFZOzs7OztJQUFaLFVBQWEsR0FBRyxFQUFFLElBQUk7UUFDcEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFHLEdBQUssRUFBRSxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUVELDhDQUFhOzs7O0lBQWIsVUFBYyxHQUFHO1FBQ2YsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFHLEdBQUssRUFBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7O2dCQXpCRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVJRLFVBQVU7OztpQ0FEbkI7Q0FrQ0MsQUEzQkQsSUEyQkM7U0F4Qlksc0JBQXNCOzs7Ozs7SUFFckIsc0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5cbmNvbnN0IGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKTtcbmhlYWRlcnMuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBQcmFjdGljZVBsYW5uZXJTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG4gIGdldFJlcXVlc3QodXJsKSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dXJsfWAsIHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgfVxuXG4gIHBvc3RSZXF1ZXN0KHVybCwgZGF0YSkge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdChgJHt1cmx9YCwgZGF0YSwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xuICB9XG5cbiAgcHV0UmVxdWVzdCh1cmwsIGRhdGEpIHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnB1dChgJHt1cmx9YCwgZGF0YSwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xuICB9XG5cbiAgcGF0Y2hSZXF1ZXN0KHVybCwgZGF0YSkge1xuICAgIHJldHVybiB0aGlzLmh0dHAucGF0Y2goYCR7dXJsfWAsIGRhdGEsIHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgfVxuXG4gIHJlbW92ZVJlcXVlc3QodXJsKSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5kZWxldGUoYCR7dXJsfWAsICB7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG4gIH1cblxufVxuIl19