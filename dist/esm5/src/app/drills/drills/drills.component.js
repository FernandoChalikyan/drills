/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PracticePlannerService } from '../practice-planner.service';
var DrillsComponent = /** @class */ (function () {
    function DrillsComponent(practiceService, router, ngxSmartModalService) {
        this.practiceService = practiceService;
        this.router = router;
        this.ngxSmartModalService = ngxSmartModalService;
    }
    /**
     * @return {?}
     */
    DrillsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        console.log('-----', this.data);
    };
    /**
     * @param {?} drill
     * @param {?} index
     * @return {?}
     */
    DrillsComponent.prototype.detailClicked = /**
     * @param {?} drill
     * @param {?} index
     * @return {?}
     */
    function (drill, index) {
        this.router.navigate(['drills/detail'], { queryParams: { id: drill.id, i: index } });
    };
    /**
     * @return {?}
     */
    DrillsComponent.prototype.createCategoty = /**
     * @return {?}
     */
    function () {
        this.categoryTitle = '';
        this.actionType = 'submitCreate';
        this.ngxSmartModalService.getModal('createModal').open();
    };
    /**
     * @param {?} category
     * @param {?} i
     * @return {?}
     */
    DrillsComponent.prototype.edit = /**
     * @param {?} category
     * @param {?} i
     * @return {?}
     */
    function (category, i) {
        this.selectedIndex = i;
        this.actionType = 'submitEdit';
        this.categoryTitle = category.title;
        this.ngxSmartModalService.getModal('createModal').open();
    };
    /**
     * @param {?} data
     * @return {?}
     */
    DrillsComponent.prototype.submitEdit = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        this.practiceService.patchRequest("api/practicePlans/" + this.data[this.selectedIndex].id, data).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.data[_this.selectedIndex] = res;
            _this.categoryTitle = '';
            _this.ngxSmartModalService.getModal('createModal').close();
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.error('err', error1);
        }));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    DrillsComponent.prototype.submitCreate = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        data['items'] = [];
        this.practiceService.postRequest('api/practicePlans', data).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            console.log('res', res);
            _this.data.push(res);
            _this.categoryTitle = '';
            _this.ngxSmartModalService.getModal('createModal').close();
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.error('err', error1);
        }));
    };
    /**
     * @return {?}
     */
    DrillsComponent.prototype.submit = /**
     * @return {?}
     */
    function () {
        console.log('title', this.categoryTitle);
        /** @type {?} */
        var data = {
            title: this.categoryTitle
        };
        this[this.actionType](data);
    };
    /**
     * @param {?} i
     * @return {?}
     */
    DrillsComponent.prototype.remove = /**
     * @param {?} i
     * @return {?}
     */
    function (i) {
        this.selectedIndex = i;
        this.ngxSmartModalService.getModal('removeModal').open();
    };
    /**
     * @return {?}
     */
    DrillsComponent.prototype.submitRemove = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.practiceService.removeRequest("api/practicePlans/" + this.data[this.selectedIndex].id).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            _this.data.splice(_this.selectedIndex, 1);
            _this.ngxSmartModalService.getModal('removeModal').close();
            _this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        function (error1) {
            console.error('err', error1);
        }));
    };
    DrillsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-drills',
                    template: "<div *ngIf=\"data\">\n  <div class=\"add-btn btn btn-main mb-15\" (click)=\"createCategoty()\"> Add Category </div>\n\n  <div *ngFor=\"let categoryDrill of data; let i = index\" class=\"categories mb-15\">\n    <p (click)=\"detailClicked(categoryDrill, i)\">\n      {{categoryDrill.title}}\n    </p>\n    <div class=\"actions\">\n      <span class=\"edit glyphicon glyphicon-pencil\" title=\"Edit\" (click)=\"edit(categoryDrill, i)\"></span>\n      <span class=\"delete glyphicon glyphicon-trash\" title=\"Remove\" (click)=\"remove(i)\"></span>\n    </div>\n  </div>\n</div>\n\n<ngx-smart-modal #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal #createModal identifier=\"createModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitCreate'\">Create New Category</p>\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitEdit'\">Edit Category</p>\n    <p>\n      <input type=\"text\" placeholder=\"Category Title\" [(ngModel)]=\"categoryTitle\" class=\"form-control\">\n    </p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"createModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submit()\">Create</button>\n    </div>\n\n  </div>\n</ngx-smart-modal>\n",
                    styles: [".categories{position:relative;border-radius:3px;color:#000;background:#fff;padding:5px;border:1px solid #ccc}.categories .actions{position:absolute;top:0;right:0;padding:5px}.categories .actions span{padding:0 5px;cursor:pointer}.categories p{margin-bottom:0}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}"]
                }] }
    ];
    /** @nocollapse */
    DrillsComponent.ctorParameters = function () { return [
        { type: PracticePlannerService },
        { type: Router },
        { type: NgxSmartModalService }
    ]; };
    DrillsComponent.propDecorators = {
        data: [{ type: Input }]
    };
    return DrillsComponent;
}());
export { DrillsComponent };
if (false) {
    /** @type {?} */
    DrillsComponent.prototype.data;
    /** @type {?} */
    DrillsComponent.prototype.categoryTitle;
    /** @type {?} */
    DrillsComponent.prototype.selectedIndex;
    /** @type {?} */
    DrillsComponent.prototype.actionType;
    /**
     * @type {?}
     * @private
     */
    DrillsComponent.prototype.practiceService;
    /**
     * @type {?}
     * @private
     */
    DrillsComponent.prototype.router;
    /** @type {?} */
    DrillsComponent.prototype.ngxSmartModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJpbGxzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2RyaWxsLyIsInNvdXJjZXMiOlsic3JjL2FwcC9kcmlsbHMvZHJpbGxzL2RyaWxscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUVyRTtJQVlFLHlCQUVVLGVBQXVDLEVBQ3ZDLE1BQWMsRUFDZixvQkFBMEM7UUFGekMsb0JBQWUsR0FBZixlQUFlLENBQXdCO1FBQ3ZDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO0lBQzdDLENBQUM7Ozs7SUFFUCxrQ0FBUTs7O0lBQVI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRUQsdUNBQWE7Ozs7O0lBQWIsVUFBYyxLQUFLLEVBQUUsS0FBSztRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxFQUFFLEVBQUMsV0FBVyxFQUFFLEVBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7O0lBRUQsd0NBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUM7UUFDakMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFFRCw4QkFBSTs7Ozs7SUFBSixVQUFLLFFBQVEsRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxZQUFZLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0QsQ0FBQzs7Ozs7SUFFRCxvQ0FBVTs7OztJQUFWLFVBQVcsSUFBSTtRQUFmLGlCQVdDO1FBVkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsdUJBQXFCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O1FBQ3hHLFVBQUMsR0FBUTtZQUNQLEtBQUksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEdBQUcsQ0FBQztZQUNwQyxLQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN4QixLQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVELENBQUM7Ozs7UUFDRCxVQUFBLE1BQU07WUFDSixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ0YsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsc0NBQVk7Ozs7SUFBWixVQUFjLElBQUk7UUFBbEIsaUJBYUM7UUFaQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDbkUsVUFBQyxHQUFRO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDeEIsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDcEIsS0FBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDeEIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1RCxDQUFDOzs7O1FBQ0QsVUFBQSxNQUFNO1lBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsZ0NBQU07OztJQUFOO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDOztZQUNyQyxJQUFJLEdBQUc7WUFDVCxLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7U0FDMUI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRUQsZ0NBQU07Ozs7SUFBTixVQUFPLENBQUM7UUFDTixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNELENBQUM7Ozs7SUFFRCxzQ0FBWTs7O0lBQVo7UUFBQSxpQkFXQztRQVZDLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLHVCQUFxQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFJLENBQUMsQ0FBQyxTQUFTOzs7O1FBQ25HLFVBQUMsR0FBUTtZQUNQLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMxRCxLQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQztRQUMzQixDQUFDOzs7O1FBQ0QsVUFBQSxNQUFNO1lBQ0osT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOztnQkE1RkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0Qix1cURBQXNDOztpQkFFdkM7Ozs7Z0JBTlEsc0JBQXNCO2dCQUZ0QixNQUFNO2dCQUNOLG9CQUFvQjs7O3VCQVUxQixLQUFLOztJQXVGUixzQkFBQztDQUFBLEFBOUZELElBOEZDO1NBekZZLGVBQWU7OztJQUUxQiwrQkFBbUI7O0lBQ25CLHdDQUE2Qjs7SUFDN0Isd0NBQTZCOztJQUM3QixxQ0FBMEI7Ozs7O0lBSXhCLDBDQUErQzs7Ozs7SUFDL0MsaUNBQXNCOztJQUN0QiwrQ0FBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsU2VydmljZSB9IGZyb20gJ25neC1zbWFydC1tb2RhbCc7XG5pbXBvcnQgeyBQcmFjdGljZVBsYW5uZXJTZXJ2aWNlIH0gZnJvbSAnLi4vcHJhY3RpY2UtcGxhbm5lci5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWRyaWxscycsXG4gIHRlbXBsYXRlVXJsOiAnLi9kcmlsbHMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9kcmlsbHMuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIERyaWxsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZGF0YTogYW55O1xuICBwdWJsaWMgY2F0ZWdvcnlUaXRsZTogc3RyaW5nO1xuICBwdWJsaWMgc2VsZWN0ZWRJbmRleDogbnVtYmVyO1xuICBwdWJsaWMgYWN0aW9uVHlwZTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIC8vIHByaXZhdGUgbG9jYWxTZXJ2aWNlOiBMb2NhbFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBwcmFjdGljZVNlcnZpY2U6IFByYWN0aWNlUGxhbm5lclNlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwdWJsaWMgbmd4U21hcnRNb2RhbFNlcnZpY2U6IE5neFNtYXJ0TW9kYWxTZXJ2aWNlLFxuICAgICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgY29uc29sZS5sb2coJy0tLS0tJywgdGhpcy5kYXRhKTtcbiAgfVxuXG4gIGRldGFpbENsaWNrZWQoZHJpbGwsIGluZGV4KTogdm9pZCB7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydkcmlsbHMvZGV0YWlsJ10sIHtxdWVyeVBhcmFtczoge2lkOiBkcmlsbC5pZCwgaTogaW5kZXggfX0pO1xuICB9XG5cbiAgY3JlYXRlQ2F0ZWdvdHkoKTogdm9pZCB7XG4gICAgdGhpcy5jYXRlZ29yeVRpdGxlID0gJyc7XG4gICAgdGhpcy5hY3Rpb25UeXBlID0gJ3N1Ym1pdENyZWF0ZSc7XG4gICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgnY3JlYXRlTW9kYWwnKS5vcGVuKCk7XG4gIH1cblxuICBlZGl0KGNhdGVnb3J5LCBpKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gaTtcbiAgICB0aGlzLmFjdGlvblR5cGUgPSAnc3VibWl0RWRpdCc7XG4gICAgdGhpcy5jYXRlZ29yeVRpdGxlID0gY2F0ZWdvcnkudGl0bGU7XG4gICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgnY3JlYXRlTW9kYWwnKS5vcGVuKCk7XG4gIH1cblxuICBzdWJtaXRFZGl0KGRhdGEpOiB2b2lkIHtcbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5wYXRjaFJlcXVlc3QoYGFwaS9wcmFjdGljZVBsYW5zLyR7dGhpcy5kYXRhW3RoaXMuc2VsZWN0ZWRJbmRleF0uaWR9YCwgZGF0YSkuc3Vic2NyaWJlKFxuICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuZGF0YVt0aGlzLnNlbGVjdGVkSW5kZXhdID0gcmVzO1xuICAgICAgICB0aGlzLmNhdGVnb3J5VGl0bGUgPSAnJztcbiAgICAgICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgnY3JlYXRlTW9kYWwnKS5jbG9zZSgpO1xuICAgICAgfSxcbiAgICAgIGVycm9yMSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ2VycicsIGVycm9yMSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHN1Ym1pdENyZWF0ZSAoZGF0YSk6IHZvaWQge1xuICAgIGRhdGFbJ2l0ZW1zJ10gPSBbXTtcbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5wb3N0UmVxdWVzdCgnYXBpL3ByYWN0aWNlUGxhbnMnLCBkYXRhKS5zdWJzY3JpYmUoXG4gICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ3JlcycsIHJlcyk7XG4gICAgICAgIHRoaXMuZGF0YS5wdXNoKHJlcyk7XG4gICAgICAgIHRoaXMuY2F0ZWdvcnlUaXRsZSA9ICcnO1xuICAgICAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdjcmVhdGVNb2RhbCcpLmNsb3NlKCk7XG4gICAgICB9LFxuICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignZXJyJywgZXJyb3IxKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgc3VibWl0KCk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKCd0aXRsZScsIHRoaXMuY2F0ZWdvcnlUaXRsZSk7XG4gICAgbGV0IGRhdGEgPSB7XG4gICAgICB0aXRsZTogdGhpcy5jYXRlZ29yeVRpdGxlXG4gICAgfTtcbiAgICB0aGlzW3RoaXMuYWN0aW9uVHlwZV0oZGF0YSk7XG4gIH1cblxuICByZW1vdmUoaSk6IHZvaWQge1xuICAgIHRoaXMuc2VsZWN0ZWRJbmRleCA9IGk7XG4gICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgncmVtb3ZlTW9kYWwnKS5vcGVuKCk7XG4gIH1cblxuICBzdWJtaXRSZW1vdmUoKTogdm9pZCB7XG4gICAgdGhpcy5wcmFjdGljZVNlcnZpY2UucmVtb3ZlUmVxdWVzdChgYXBpL3ByYWN0aWNlUGxhbnMvJHt0aGlzLmRhdGFbdGhpcy5zZWxlY3RlZEluZGV4XS5pZH1gKS5zdWJzY3JpYmUoXG4gICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5kYXRhLnNwbGljZSh0aGlzLnNlbGVjdGVkSW5kZXgsIDEpO1xuICAgICAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdyZW1vdmVNb2RhbCcpLmNsb3NlKCk7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRJbmRleCA9IE5hTjtcbiAgICAgIH0sXG4gICAgICBlcnJvcjEgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdlcnInLCBlcnJvcjEpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxufVxuIl19