/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PracticePlannerService } from '../practice-planner.service';
export class DrillsComponent {
    /**
     * @param {?} practiceService
     * @param {?} router
     * @param {?} ngxSmartModalService
     */
    constructor(practiceService, router, ngxSmartModalService) {
        this.practiceService = practiceService;
        this.router = router;
        this.ngxSmartModalService = ngxSmartModalService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        console.log('-----', this.data);
    }
    /**
     * @param {?} drill
     * @param {?} index
     * @return {?}
     */
    detailClicked(drill, index) {
        this.router.navigate(['drills/detail'], { queryParams: { id: drill.id, i: index } });
    }
    /**
     * @return {?}
     */
    createCategoty() {
        this.categoryTitle = '';
        this.actionType = 'submitCreate';
        this.ngxSmartModalService.getModal('createModal').open();
    }
    /**
     * @param {?} category
     * @param {?} i
     * @return {?}
     */
    edit(category, i) {
        this.selectedIndex = i;
        this.actionType = 'submitEdit';
        this.categoryTitle = category.title;
        this.ngxSmartModalService.getModal('createModal').open();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    submitEdit(data) {
        this.practiceService.patchRequest(`api/practicePlans/${this.data[this.selectedIndex].id}`, data).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.data[this.selectedIndex] = res;
            this.categoryTitle = '';
            this.ngxSmartModalService.getModal('createModal').close();
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.error('err', error1);
        }));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    submitCreate(data) {
        data['items'] = [];
        this.practiceService.postRequest('api/practicePlans', data).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            console.log('res', res);
            this.data.push(res);
            this.categoryTitle = '';
            this.ngxSmartModalService.getModal('createModal').close();
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.error('err', error1);
        }));
    }
    /**
     * @return {?}
     */
    submit() {
        console.log('title', this.categoryTitle);
        /** @type {?} */
        let data = {
            title: this.categoryTitle
        };
        this[this.actionType](data);
    }
    /**
     * @param {?} i
     * @return {?}
     */
    remove(i) {
        this.selectedIndex = i;
        this.ngxSmartModalService.getModal('removeModal').open();
    }
    /**
     * @return {?}
     */
    submitRemove() {
        this.practiceService.removeRequest(`api/practicePlans/${this.data[this.selectedIndex].id}`).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.data.splice(this.selectedIndex, 1);
            this.ngxSmartModalService.getModal('removeModal').close();
            this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.error('err', error1);
        }));
    }
}
DrillsComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-drills',
                template: "<div *ngIf=\"data\">\n  <div class=\"add-btn btn btn-main mb-15\" (click)=\"createCategoty()\"> Add Category </div>\n\n  <div *ngFor=\"let categoryDrill of data; let i = index\" class=\"categories mb-15\">\n    <p (click)=\"detailClicked(categoryDrill, i)\">\n      {{categoryDrill.title}}\n    </p>\n    <div class=\"actions\">\n      <span class=\"edit glyphicon glyphicon-pencil\" title=\"Edit\" (click)=\"edit(categoryDrill, i)\"></span>\n      <span class=\"delete glyphicon glyphicon-trash\" title=\"Remove\" (click)=\"remove(i)\"></span>\n    </div>\n  </div>\n</div>\n\n<ngx-smart-modal #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal #createModal identifier=\"createModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitCreate'\">Create New Category</p>\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitEdit'\">Edit Category</p>\n    <p>\n      <input type=\"text\" placeholder=\"Category Title\" [(ngModel)]=\"categoryTitle\" class=\"form-control\">\n    </p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"createModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submit()\">Create</button>\n    </div>\n\n  </div>\n</ngx-smart-modal>\n",
                styles: [".categories{position:relative;border-radius:3px;color:#000;background:#fff;padding:5px;border:1px solid #ccc}.categories .actions{position:absolute;top:0;right:0;padding:5px}.categories .actions span{padding:0 5px;cursor:pointer}.categories p{margin-bottom:0}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}"]
            }] }
];
/** @nocollapse */
DrillsComponent.ctorParameters = () => [
    { type: PracticePlannerService },
    { type: Router },
    { type: NgxSmartModalService }
];
DrillsComponent.propDecorators = {
    data: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DrillsComponent.prototype.data;
    /** @type {?} */
    DrillsComponent.prototype.categoryTitle;
    /** @type {?} */
    DrillsComponent.prototype.selectedIndex;
    /** @type {?} */
    DrillsComponent.prototype.actionType;
    /**
     * @type {?}
     * @private
     */
    DrillsComponent.prototype.practiceService;
    /**
     * @type {?}
     * @private
     */
    DrillsComponent.prototype.router;
    /** @type {?} */
    DrillsComponent.prototype.ngxSmartModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJpbGxzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2RyaWxsLyIsInNvdXJjZXMiOlsic3JjL2FwcC9kcmlsbHMvZHJpbGxzL2RyaWxscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQU9yRSxNQUFNLE9BQU8sZUFBZTs7Ozs7O0lBTzFCLFlBRVUsZUFBdUMsRUFDdkMsTUFBYyxFQUNmLG9CQUEwQztRQUZ6QyxvQkFBZSxHQUFmLGVBQWUsQ0FBd0I7UUFDdkMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNmLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7SUFDN0MsQ0FBQzs7OztJQUVQLFFBQVE7UUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQUssRUFBRSxLQUFLO1FBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsZUFBZSxDQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUUsRUFBQyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUMsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUM7UUFDakMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFFRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDZCxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQztRQUMvQixJQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxJQUFJO1FBQ2IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMscUJBQXFCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDeEcsQ0FBQyxHQUFRLEVBQUUsRUFBRTtZQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEdBQUcsQ0FBQztZQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVELENBQUM7Ozs7UUFDRCxNQUFNLENBQUMsRUFBRTtZQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFDRixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUUsSUFBSTtRQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDbkUsQ0FBQyxHQUFRLEVBQUUsRUFBRTtZQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUQsQ0FBQzs7OztRQUNELE1BQU0sQ0FBQyxFQUFFO1lBQ1AsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsTUFBTTtRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs7WUFDckMsSUFBSSxHQUFHO1lBQ1QsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhO1NBQzFCO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxDQUFDO1FBQ04sSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLHFCQUFxQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDbkcsQ0FBQyxHQUFRLEVBQUUsRUFBRTtZQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMxRCxJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsQ0FBQztRQUMzQixDQUFDOzs7O1FBQ0QsTUFBTSxDQUFDLEVBQUU7WUFDUCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ0YsQ0FBQztJQUNKLENBQUM7OztZQTVGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLHVxREFBc0M7O2FBRXZDOzs7O1lBTlEsc0JBQXNCO1lBRnRCLE1BQU07WUFDTixvQkFBb0I7OzttQkFVMUIsS0FBSzs7OztJQUFOLCtCQUFtQjs7SUFDbkIsd0NBQTZCOztJQUM3Qix3Q0FBNkI7O0lBQzdCLHFDQUEwQjs7Ozs7SUFJeEIsMENBQStDOzs7OztJQUMvQyxpQ0FBc0I7O0lBQ3RCLCtDQUFpRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE5neFNtYXJ0TW9kYWxTZXJ2aWNlIH0gZnJvbSAnbmd4LXNtYXJ0LW1vZGFsJztcbmltcG9ydCB7IFByYWN0aWNlUGxhbm5lclNlcnZpY2UgfSBmcm9tICcuLi9wcmFjdGljZS1wbGFubmVyLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtZHJpbGxzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RyaWxscy5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2RyaWxscy5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRHJpbGxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBkYXRhOiBhbnk7XG4gIHB1YmxpYyBjYXRlZ29yeVRpdGxlOiBzdHJpbmc7XG4gIHB1YmxpYyBzZWxlY3RlZEluZGV4OiBudW1iZXI7XG4gIHB1YmxpYyBhY3Rpb25UeXBlOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgLy8gcHJpdmF0ZSBsb2NhbFNlcnZpY2U6IExvY2FsU2VydmljZSxcbiAgICBwcml2YXRlIHByYWN0aWNlU2VydmljZTogUHJhY3RpY2VQbGFubmVyU2VydmljZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHB1YmxpYyBuZ3hTbWFydE1vZGFsU2VydmljZTogTmd4U21hcnRNb2RhbFNlcnZpY2UsXG4gICAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBjb25zb2xlLmxvZygnLS0tLS0nLCB0aGlzLmRhdGEpO1xuICB9XG5cbiAgZGV0YWlsQ2xpY2tlZChkcmlsbCwgaW5kZXgpOiB2b2lkIHtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ2RyaWxscy9kZXRhaWwnXSwge3F1ZXJ5UGFyYW1zOiB7aWQ6IGRyaWxsLmlkLCBpOiBpbmRleCB9fSk7XG4gIH1cblxuICBjcmVhdGVDYXRlZ290eSgpOiB2b2lkIHtcbiAgICB0aGlzLmNhdGVnb3J5VGl0bGUgPSAnJztcbiAgICB0aGlzLmFjdGlvblR5cGUgPSAnc3VibWl0Q3JlYXRlJztcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdjcmVhdGVNb2RhbCcpLm9wZW4oKTtcbiAgfVxuXG4gIGVkaXQoY2F0ZWdvcnksIGkpOiB2b2lkIHtcbiAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBpO1xuICAgIHRoaXMuYWN0aW9uVHlwZSA9ICdzdWJtaXRFZGl0JztcbiAgICB0aGlzLmNhdGVnb3J5VGl0bGUgPSBjYXRlZ29yeS50aXRsZTtcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdjcmVhdGVNb2RhbCcpLm9wZW4oKTtcbiAgfVxuXG4gIHN1Ym1pdEVkaXQoZGF0YSk6IHZvaWQge1xuICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLnBhdGNoUmVxdWVzdChgYXBpL3ByYWN0aWNlUGxhbnMvJHt0aGlzLmRhdGFbdGhpcy5zZWxlY3RlZEluZGV4XS5pZH1gLCBkYXRhKS5zdWJzY3JpYmUoXG4gICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5kYXRhW3RoaXMuc2VsZWN0ZWRJbmRleF0gPSByZXM7XG4gICAgICAgIHRoaXMuY2F0ZWdvcnlUaXRsZSA9ICcnO1xuICAgICAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdjcmVhdGVNb2RhbCcpLmNsb3NlKCk7XG4gICAgICB9LFxuICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcignZXJyJywgZXJyb3IxKTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgc3VibWl0Q3JlYXRlIChkYXRhKTogdm9pZCB7XG4gICAgZGF0YVsnaXRlbXMnXSA9IFtdO1xuICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLnBvc3RSZXF1ZXN0KCdhcGkvcHJhY3RpY2VQbGFucycsIGRhdGEpLnN1YnNjcmliZShcbiAgICAgIChyZXM6IGFueSkgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZygncmVzJywgcmVzKTtcbiAgICAgICAgdGhpcy5kYXRhLnB1c2gocmVzKTtcbiAgICAgICAgdGhpcy5jYXRlZ29yeVRpdGxlID0gJyc7XG4gICAgICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ2NyZWF0ZU1vZGFsJykuY2xvc2UoKTtcbiAgICAgIH0sXG4gICAgICBlcnJvcjEgPT4ge1xuICAgICAgICBjb25zb2xlLmVycm9yKCdlcnInLCBlcnJvcjEpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBzdWJtaXQoKTogdm9pZCB7XG4gICAgY29uc29sZS5sb2coJ3RpdGxlJywgdGhpcy5jYXRlZ29yeVRpdGxlKTtcbiAgICBsZXQgZGF0YSA9IHtcbiAgICAgIHRpdGxlOiB0aGlzLmNhdGVnb3J5VGl0bGVcbiAgICB9O1xuICAgIHRoaXNbdGhpcy5hY3Rpb25UeXBlXShkYXRhKTtcbiAgfVxuXG4gIHJlbW92ZShpKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gaTtcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdyZW1vdmVNb2RhbCcpLm9wZW4oKTtcbiAgfVxuXG4gIHN1Ym1pdFJlbW92ZSgpOiB2b2lkIHtcbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5yZW1vdmVSZXF1ZXN0KGBhcGkvcHJhY3RpY2VQbGFucy8ke3RoaXMuZGF0YVt0aGlzLnNlbGVjdGVkSW5kZXhdLmlkfWApLnN1YnNjcmliZShcbiAgICAgIChyZXM6IGFueSkgPT4ge1xuICAgICAgICB0aGlzLmRhdGEuc3BsaWNlKHRoaXMuc2VsZWN0ZWRJbmRleCwgMSk7XG4gICAgICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ3JlbW92ZU1vZGFsJykuY2xvc2UoKTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gTmFOO1xuICAgICAgfSxcbiAgICAgIGVycm9yMSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ2VycicsIGVycm9yMSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG59XG4iXX0=