/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'ng2-dropdown';
import { PracticePlannerService } from './practice-planner.service';
import { DrillsComponent } from './drills/drills.component';
import { DrillsDetailsComponent } from './drills-details/drills-details.component';
export class DrillsModule {
}
DrillsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [DrillsComponent, DrillsDetailsComponent],
                imports: [
                    CommonModule,
                    NgxSmartModalModule.forRoot(),
                    FormsModule,
                    DropdownModule
                ],
                providers: [
                    DatePipe,
                    PracticePlannerService
                ],
                exports: [
                    DrillsComponent,
                    DrillsDetailsComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJpbGxzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2RyaWxsLyIsInNvdXJjZXMiOlsic3JjL2FwcC9kcmlsbHMvZHJpbGxzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzlDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRXBFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQW1CbkYsTUFBTSxPQUFPLFlBQVk7OztZQWpCeEIsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGVBQWUsRUFBRSxzQkFBc0IsQ0FBQztnQkFDdkQsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osbUJBQW1CLENBQUMsT0FBTyxFQUFFO29CQUM3QixXQUFXO29CQUNYLGNBQWM7aUJBQ2Y7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULFFBQVE7b0JBQ1Isc0JBQXNCO2lCQUN2QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsZUFBZTtvQkFDZixzQkFBc0I7aUJBQ3ZCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlLCBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsTW9kdWxlIH0gZnJvbSAnbmd4LXNtYXJ0LW1vZGFsJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgRHJvcGRvd25Nb2R1bGUgfSBmcm9tICduZzItZHJvcGRvd24nO1xuaW1wb3J0IHsgUHJhY3RpY2VQbGFubmVyU2VydmljZSB9IGZyb20gJy4vcHJhY3RpY2UtcGxhbm5lci5zZXJ2aWNlJztcblxuaW1wb3J0IHsgRHJpbGxzQ29tcG9uZW50IH0gZnJvbSAnLi9kcmlsbHMvZHJpbGxzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEcmlsbHNEZXRhaWxzQ29tcG9uZW50IH0gZnJvbSAnLi9kcmlsbHMtZGV0YWlscy9kcmlsbHMtZGV0YWlscy5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtEcmlsbHNDb21wb25lbnQsIERyaWxsc0RldGFpbHNDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIE5neFNtYXJ0TW9kYWxNb2R1bGUuZm9yUm9vdCgpLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIERyb3Bkb3duTW9kdWxlXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIERhdGVQaXBlLFxuICAgIFByYWN0aWNlUGxhbm5lclNlcnZpY2VcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIERyaWxsc0NvbXBvbmVudCxcbiAgICBEcmlsbHNEZXRhaWxzQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRHJpbGxzTW9kdWxlIHsgfVxuIl19