/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PracticePlannerService } from '../practice-planner.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/filter';
export class DrillsDetailsComponent {
    /**
     * @param {?} practiceService
     * @param {?} route
     * @param {?} router
     * @param {?} datePipe
     * @param {?} sanitizer
     * @param {?} ngxSmartModalService
     */
    constructor(practiceService, route, router, datePipe, sanitizer, ngxSmartModalService) {
        this.practiceService = practiceService;
        this.route = route;
        this.router = router;
        this.datePipe = datePipe;
        this.sanitizer = sanitizer;
        this.ngxSmartModalService = ngxSmartModalService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        let drillIndex;
        this.route.queryParams
            .filter((/**
         * @param {?} params
         * @return {?}
         */
        params => params.id))
            .subscribe((/**
         * @param {?} params
         * @return {?}
         */
        params => {
            drillIndex = params.id;
        }));
        this.practiceService.getRequest(`api/practicePlans/${drillIndex}`).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.drill = res;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.log('errr', error1);
        }));
        this.practiceService.getRequest('api/saveplans').subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.plans = res;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.log('error', error1);
        }));
    }
    /**
     * @return {?}
     */
    back() {
        this.router.navigate(['drills']);
    }
    /**
     * @param {?} index
     * @return {?}
     */
    remove(index) {
        this.ngxSmartModalService.getModal('removeModal').open();
        this.selectedIndex = index;
    }
    /**
     * @return {?}
     */
    submitRemove() {
        this.drill.items.splice(this.selectedIndex, 1);
        this.practiceService.putRequest(`api/practicePlans/${this.drill.id}`, this.drill).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.ngxSmartModalService.getModal('removeModal').close();
            this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.log('remove error', error1);
        }));
    }
    /**
     * @param {?} drill
     * @param {?} i
     * @return {?}
     */
    edit(drill, i) {
        this.selectedDrill = drill;
        this.selectedIndex = i;
        this.ngxSmartModalService.getModal('editModal').open();
    }
    /**
     * @return {?}
     */
    submitEdit() {
        if (this.selectedIndex || !isNaN(this.selectedIndex)) {
            this.drill.items[this.selectedIndex] = this.selectedDrill;
        }
        else {
            this.drill.items.push(this.selectedDrill);
        }
        this.practiceService.putRequest(`api/practicePlans/${this.drill.id}`, this.drill).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            this.ngxSmartModalService.getModal('editModal').close();
            this.selectedIndex = NaN;
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.error('edit error', error1);
        }));
    }
    /**
     * @return {?}
     */
    create() {
        this.selectedDrill = {
            title: '',
            descriptionA: '',
            descriptionB: '',
            descriptionC: '',
            descriptionD: '',
            descriptionE: '',
            descriptionF: '',
            videoURL: '',
            id: this.datePipe.transform(new Date(), 'yyyy MM dd HH MM SS MM').replace(/\s/g, '')
        };
        this.ngxSmartModalService.getModal('editModal').open();
    }
    /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    check(plan, drill) {
        /** @type {?} */
        let checked = plan.description[0].find((/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            return item.id === drill.id;
        }));
        return checked ? true : false;
    }
    /**
     * @param {?} plan
     * @param {?} drill
     * @return {?}
     */
    addToPlan(plan, drill) {
        /** @type {?} */
        let reomveIndex;
        /** @type {?} */
        let checked = plan.description[0].find((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        (item, index) => {
            if (item.id === drill.id) {
                reomveIndex = index;
            }
            return item.id === drill.id;
        }));
        if (checked) {
            plan.description[0].splice(reomveIndex, 1);
            console.log('remove', plan);
        }
        else {
            plan.description[0].push(drill);
            console.log('add', plan);
        }
        this.practiceService.putRequest(`api/saveplans/${plan.id}`, plan).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            console.log('added', res);
        }), (/**
         * @param {?} error1
         * @return {?}
         */
        error1 => {
            console.error('err', error1);
        }));
    }
}
DrillsDetailsComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-drills-details',
                template: "<div *ngIf=\"drill\" class=\"\">\n  <div>\n    <div class=\"back\">\n      <button (click)=\"back()\" type=\"button\" class=\"btn btn-primary\">\n        <img src=\"../../../assets/images/60577.png\" alt=\"\">\n        Back\n      </button>\n      <button (click)=\"create()\" class=\"btn btn-main\">Create Drill</button>\n    </div>\n  </div>\n  <h2 class=\"text-center main-title\">{{drill.title}}</h2>\n  <div *ngFor=\"let item of drill.items; let i = index\" class=\"drill row\">\n      <div class=\"drill-head-section flexbox\">\n        <h3 class=\"drill-heading m-0\">{{item.title}}</h3>\n        <div class=\"actions\">\n          <span class=\"glyphicon glyphicon-pencil btn\" title=\"Edit\" (click)=\"edit(item, i)\"></span>\n          <span class=\"glyphicon glyphicon-trash btn\" title=\"Remove\" (click)=\"remove(i)\"></span>\n          <span class=\"dropdown\" dropdown>\n            <button class=\"btn btn-link\" dropdown-open>Add to plan</button>\n            <ul class=\"dropdown-menu\" dropdown-not-closable-zone>\n              <li *ngFor=\"let plan of plans\">\n                <input type=\"checkbox\" [checked]=\"check(plan, item)\" (change)=\"addToPlan(plan, item)\">\n                <label>\n                  {{plan.description[1].name}}\n                </label>\n              </li>\n            </ul>\n          </span>\n        </div>\n      </div>\n      <div class=\"drill-content\">\n        <p *ngIf=\"item.descriptionA\">{{item.descriptionA}}</p>\n        <p *ngIf=\"item.descriptionB\">{{item.descriptionB}}</p>\n        <p *ngIf=\"item.descriptionC\">{{item.descriptionC}}</p>\n        <p *ngIf=\"item.descriptionD\">{{item.descriptionD}}</p>\n        <p *ngIf=\"item.descriptionE\">{{item.descriptionE}}</p>\n        <p *ngIf=\"item.descriptionF\">{{item.descriptionF}}</p>\n        <p class=\"mb-0\">\n          <iframe width=\"520\" height=\"310\" class=\"e2e-iframe-trusted-src\" [src]=\"sanitizer.bypassSecurityTrustResourceUrl(item.videoURL)\" frameborder=\"0\" allowfullscreen></iframe>\n        </p>\n      </div>\n  </div>\n</div>\n<ngx-smart-modal  *ngIf=\"drill\" #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal  *ngIf=\"drill\" #editModal identifier=\"editModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Edit Drill</p>\n   <div *ngIf=\"selectedDrill\">\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Title</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.title\" name=\"\" id=\"title\" cols=\"30\" rows=\"3\">{{selectedDrill.title}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description A</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionA\" name=\"\" id=\"descriptionA\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionA}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description B</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionB\" name=\"\" id=\"descriptionB\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionB}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description C</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionC\" name=\"\" id=\"descriptionC\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionC}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description D</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionD\" name=\"\" id=\"descriptionD\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionD}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description E</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionE\" name=\"\" id=\"descriptionE\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionE}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description F</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionF\" name=\"\" id=\"descriptionF\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionF}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Video Url</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.videoURL\" name=\"\" id=\"videoURL\" cols=\"30\" rows=\"3\">{{selectedDrill.videoURL}}</textarea>\n         </div>\n       </div>\n   </div>\n\n  <div class=\"text-center\">\n    <button class=\"btn btn-danger\" (click)=\"editModal.close()\">Cancel</button>\n    <button class=\"btn btn-primary\" (click)=\"submitEdit()\">Save</button>\n  </div>\n  </div>\n</ngx-smart-modal>\n",
                styles: [".drill{margin:25px auto;background:#fff}.drill .actions span{display:inline-block;margin-left:10px;padding:0}.actions .btn:hover{color:#fff}.actions .btn:active{box-shadow:none}.actions .btn:focus{color:#fff;outline:0}.btn-link{color:#fff;text-decoration:underline}.back img{height:17px}.back .btn{margin-right:10px}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}.modal-content-inner .form-control{resize:vertical}.main-title{color:#035596}.glyphicon{top:0}.dropdown-menu{top:40px;right:0;left:auto;padding:10px 5px;margin:5px 0}.dropdown-menu li{display:flex;align-items:center;margin-bottom:5px}.dropdown-menu li input{margin:0 5px 0 0}.dropdown-menu li label{margin-bottom:0;color:#000;font-weight:500}.input-group-addon{width:110px}.btn-primary{font-weight:600;text-transform:uppercase;border-color:transparent;background-color:#0070c9}.btn-primary:hover{background-color:#035596}.drill-head-section{padding:15px;background:#0070c9;color:#fff;border-top-left-radius:4px;border-top-right-radius:4px;justify-content:space-between}.drill-heading{color:#fff;font-size:18px;line-height:unset;padding:4px 0}.drill-content{padding:10px 20px 30px}.mb-0{margin-bottom:0}iframe{width:100%}"]
            }] }
];
/** @nocollapse */
DrillsDetailsComponent.ctorParameters = () => [
    { type: PracticePlannerService },
    { type: ActivatedRoute },
    { type: Router },
    { type: DatePipe },
    { type: DomSanitizer },
    { type: NgxSmartModalService }
];
DrillsDetailsComponent.propDecorators = {
    data: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DrillsDetailsComponent.prototype.data;
    /** @type {?} */
    DrillsDetailsComponent.prototype.drill;
    /** @type {?} */
    DrillsDetailsComponent.prototype.plans;
    /** @type {?} */
    DrillsDetailsComponent.prototype.selectedDrill;
    /** @type {?} */
    DrillsDetailsComponent.prototype.selectedIndex;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.practiceService;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    DrillsDetailsComponent.prototype.datePipe;
    /** @type {?} */
    DrillsDetailsComponent.prototype.sanitizer;
    /** @type {?} */
    DrillsDetailsComponent.prototype.ngxSmartModalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJpbGxzLWRldGFpbHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZHJpbGwvIiwic291cmNlcyI6WyJzcmMvYXBwL2RyaWxscy9kcmlsbHMtZGV0YWlscy9kcmlsbHMtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLDBCQUEwQixDQUFDO0FBT2xDLE1BQU0sT0FBTyxzQkFBc0I7Ozs7Ozs7OztJQVFqQyxZQUNVLGVBQXVDLEVBQ3ZDLEtBQXFCLEVBQ3JCLE1BQWMsRUFDZCxRQUFrQixFQUNuQixTQUF1QixFQUN2QixvQkFBMEM7UUFMekMsb0JBQWUsR0FBZixlQUFlLENBQXdCO1FBQ3ZDLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ25CLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFDdkIseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtJQUMvQyxDQUFDOzs7O0lBRUwsUUFBUTs7WUFDRixVQUFVO1FBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXO2FBQ25CLE1BQU07Ozs7UUFBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUM7YUFDM0IsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2xCLFVBQVUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMscUJBQXFCLFVBQVUsRUFBRSxDQUFDLENBQUMsU0FBUzs7OztRQUMxRSxDQUFDLEdBQVEsRUFBRSxFQUFFO1lBQ1gsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDbkIsQ0FBQzs7OztRQUNELE1BQU0sQ0FBQyxFQUFFO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUNGLENBQUM7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTOzs7O1FBQ3hELENBQUMsR0FBUSxFQUFFLEVBQUU7WUFDWCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUNuQixDQUFDOzs7O1FBQ0QsTUFBTSxDQUFDLEVBQUU7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQUMvQixDQUFDLEVBQ0YsQ0FBQztJQUVMLENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLEtBQUs7UUFDVixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMscUJBQXFCLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDekYsQ0FBQyxHQUFRLEVBQUUsRUFBRTtZQUNYLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDMUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7UUFDM0IsQ0FBQzs7OztRQUNELE1BQU0sQ0FBQyxFQUFFO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOzs7Ozs7SUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCxVQUFVO1FBQ1IsSUFBSyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUNyRCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztTQUMzRDthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLHFCQUFxQixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1FBQ3pGLENBQUMsR0FBUSxFQUFFLEVBQUU7WUFDWCxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3hELElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDO1FBQzNCLENBQUM7Ozs7UUFDRCxNQUFNLENBQUMsRUFBRTtZQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3RDLENBQUMsRUFDRixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsYUFBYSxHQUFHO1lBQ25CLEtBQUssRUFBRSxFQUFFO1lBQ1QsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsWUFBWSxFQUFFLEVBQUU7WUFDaEIsUUFBUSxFQUFFLEVBQUU7WUFDWixFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBQyx3QkFBd0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUMsRUFBRSxDQUFDO1NBQ25GLENBQUM7UUFDRixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7OztJQUVELEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSzs7WUFDWCxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJOzs7O1FBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUMvQyxPQUFPLElBQUksQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUM5QixDQUFDLEVBQUU7UUFDSCxPQUFPLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDaEMsQ0FBQzs7Ozs7O0lBRUQsU0FBUyxDQUFDLElBQUksRUFBRSxLQUFLOztZQUNmLFdBQVc7O1lBQ1gsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs7Ozs7UUFBRSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNwRCxJQUFLLElBQUksQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsRUFBRTtnQkFDekIsV0FBVyxHQUFHLEtBQUssQ0FBQzthQUNyQjtZQUNILE9BQU8sSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsRUFBRSxDQUFDO1FBQzlCLENBQUMsRUFBQztRQUNGLElBQUssT0FBTyxFQUFHO1lBQ2IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztTQUMxQjtRQUNDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGlCQUFpQixJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUN6RSxDQUFDLEdBQVEsRUFBRSxFQUFFO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELE1BQU0sQ0FBQyxFQUFFO1lBQ1AsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxFQUNGLENBQUM7SUFDSixDQUFDOzs7WUEzSUosU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLDZvTEFBOEM7O2FBRS9DOzs7O1lBVlEsc0JBQXNCO1lBRHRCLGNBQWM7WUFBRSxNQUFNO1lBSXRCLFFBQVE7WUFGUixZQUFZO1lBQ1osb0JBQW9COzs7bUJBVzFCLEtBQUs7Ozs7SUFBTixzQ0FBbUI7O0lBQ25CLHVDQUFrQjs7SUFDbEIsdUNBQWtCOztJQUNsQiwrQ0FBMEI7O0lBQzFCLCtDQUE2Qjs7Ozs7SUFHM0IsaURBQStDOzs7OztJQUMvQyx1Q0FBNkI7Ozs7O0lBQzdCLHdDQUFzQjs7Ozs7SUFDdEIsMENBQTBCOztJQUMxQiwyQ0FBOEI7O0lBQzlCLHNEQUFpRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgUHJhY3RpY2VQbGFubmVyU2VydmljZSB9IGZyb20gJy4uL3ByYWN0aWNlLXBsYW5uZXIuc2VydmljZSc7XG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7IE5neFNtYXJ0TW9kYWxTZXJ2aWNlIH0gZnJvbSAnbmd4LXNtYXJ0LW1vZGFsJztcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvZmlsdGVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWRyaWxscy1kZXRhaWxzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RyaWxscy1kZXRhaWxzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZHJpbGxzLWRldGFpbHMuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIERyaWxsc0RldGFpbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGRhdGE6IGFueTtcbiAgcHVibGljIGRyaWxsOiBhbnk7XG4gIHB1YmxpYyBwbGFuczogYW55O1xuICBwdWJsaWMgc2VsZWN0ZWREcmlsbDogYW55O1xuICBwdWJsaWMgc2VsZWN0ZWRJbmRleDogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcHJhY3RpY2VTZXJ2aWNlOiBQcmFjdGljZVBsYW5uZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSBkYXRlUGlwZTogRGF0ZVBpcGUsXG4gICAgcHVibGljIHNhbml0aXplcjogRG9tU2FuaXRpemVyLFxuICAgIHB1YmxpYyBuZ3hTbWFydE1vZGFsU2VydmljZTogTmd4U21hcnRNb2RhbFNlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBsZXQgZHJpbGxJbmRleDtcbiAgICB0aGlzLnJvdXRlLnF1ZXJ5UGFyYW1zXG4gICAgICAuZmlsdGVyKHBhcmFtcyA9PiBwYXJhbXMuaWQpXG4gICAgICAuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XG4gICAgICAgIGRyaWxsSW5kZXggPSBwYXJhbXMuaWQ7XG4gICAgICB9KTtcbiAgICB0aGlzLnByYWN0aWNlU2VydmljZS5nZXRSZXF1ZXN0KGBhcGkvcHJhY3RpY2VQbGFucy8ke2RyaWxsSW5kZXh9YCkuc3Vic2NyaWJlKFxuICAgICAgKHJlczogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuZHJpbGwgPSByZXM7XG4gICAgICB9LFxuICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ2VycnInLCBlcnJvcjEpO1xuICAgICAgfVxuICAgICk7XG5cbiAgICAgdGhpcy5wcmFjdGljZVNlcnZpY2UuZ2V0UmVxdWVzdCgnYXBpL3NhdmVwbGFucycpLnN1YnNjcmliZShcbiAgICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgIHRoaXMucGxhbnMgPSByZXM7XG4gICAgICAgfSxcbiAgICAgICBlcnJvcjEgPT4ge1xuICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yJywgZXJyb3IxKTtcbiAgICAgICB9XG4gICAgICk7XG5cbiAgfVxuXG4gIGJhY2soKTogdm9pZCB7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydkcmlsbHMnXSk7XG4gIH1cblxuICByZW1vdmUoaW5kZXgpOiB2b2lkIHtcbiAgICB0aGlzLm5neFNtYXJ0TW9kYWxTZXJ2aWNlLmdldE1vZGFsKCdyZW1vdmVNb2RhbCcpLm9wZW4oKTtcbiAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBpbmRleDtcbiAgfVxuXG4gIHN1Ym1pdFJlbW92ZSgpOiB2b2lkIHtcbiAgICB0aGlzLmRyaWxsLml0ZW1zLnNwbGljZSh0aGlzLnNlbGVjdGVkSW5kZXgsIDEpO1xuICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLnB1dFJlcXVlc3QoYGFwaS9wcmFjdGljZVBsYW5zLyR7dGhpcy5kcmlsbC5pZH1gLCB0aGlzLmRyaWxsKS5zdWJzY3JpYmUoXG4gICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgncmVtb3ZlTW9kYWwnKS5jbG9zZSgpO1xuICAgICAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBOYU47XG4gICAgICB9LFxuICAgICAgZXJyb3IxID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ3JlbW92ZSBlcnJvcicsIGVycm9yMSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIGVkaXQoZHJpbGwsIGkpOiB2b2lkIHtcbiAgICB0aGlzLnNlbGVjdGVkRHJpbGwgPSBkcmlsbDtcbiAgICB0aGlzLnNlbGVjdGVkSW5kZXggPSBpO1xuICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ2VkaXRNb2RhbCcpLm9wZW4oKTtcbiAgfVxuXG4gIHN1Ym1pdEVkaXQoKTogdm9pZCB7XG4gICAgaWYgKCB0aGlzLnNlbGVjdGVkSW5kZXggfHwgIWlzTmFOKHRoaXMuc2VsZWN0ZWRJbmRleCkpIHtcbiAgICAgIHRoaXMuZHJpbGwuaXRlbXNbdGhpcy5zZWxlY3RlZEluZGV4XSA9IHRoaXMuc2VsZWN0ZWREcmlsbDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5kcmlsbC5pdGVtcy5wdXNoKHRoaXMuc2VsZWN0ZWREcmlsbCk7XG4gICAgfVxuICAgIHRoaXMucHJhY3RpY2VTZXJ2aWNlLnB1dFJlcXVlc3QoYGFwaS9wcmFjdGljZVBsYW5zLyR7dGhpcy5kcmlsbC5pZH1gLCB0aGlzLmRyaWxsKS5zdWJzY3JpYmUoXG4gICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5uZ3hTbWFydE1vZGFsU2VydmljZS5nZXRNb2RhbCgnZWRpdE1vZGFsJykuY2xvc2UoKTtcbiAgICAgICAgdGhpcy5zZWxlY3RlZEluZGV4ID0gTmFOO1xuICAgICAgfSxcbiAgICAgIGVycm9yMSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ2VkaXQgZXJyb3InLCBlcnJvcjEpO1xuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBjcmVhdGUoKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3RlZERyaWxsID0ge1xuICAgICAgdGl0bGU6ICcnLFxuICAgICAgZGVzY3JpcHRpb25BOiAnJyxcbiAgICAgIGRlc2NyaXB0aW9uQjogJycsXG4gICAgICBkZXNjcmlwdGlvbkM6ICcnLFxuICAgICAgZGVzY3JpcHRpb25EOiAnJyxcbiAgICAgIGRlc2NyaXB0aW9uRTogJycsXG4gICAgICBkZXNjcmlwdGlvbkY6ICcnLFxuICAgICAgdmlkZW9VUkw6ICcnLFxuICAgICAgaWQ6IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksJ3l5eXkgTU0gZGQgSEggTU0gU1MgTU0nKS5yZXBsYWNlKC9cXHMvZywnJylcbiAgICB9O1xuICAgIHRoaXMubmd4U21hcnRNb2RhbFNlcnZpY2UuZ2V0TW9kYWwoJ2VkaXRNb2RhbCcpLm9wZW4oKTtcbiAgfVxuXG4gIGNoZWNrKHBsYW4sIGRyaWxsKTogYm9vbGVhbiB7XG4gICAgbGV0IGNoZWNrZWQgPSBwbGFuLmRlc2NyaXB0aW9uWzBdLmZpbmQoIChpdGVtKSA9PiB7XG4gICAgICByZXR1cm4gaXRlbS5pZCA9PT0gZHJpbGwuaWQ7XG4gICAgfSApO1xuICAgIHJldHVybiBjaGVja2VkID8gdHJ1ZSA6IGZhbHNlO1xuICB9XG5cbiAgYWRkVG9QbGFuKHBsYW4sIGRyaWxsKSB7XG4gICAgbGV0IHJlb212ZUluZGV4O1xuICAgIGxldCBjaGVja2VkID0gcGxhbi5kZXNjcmlwdGlvblswXS5maW5kKCAoaXRlbSwgaW5kZXgpID0+IHtcbiAgICAgICAgaWYgKCBpdGVtLmlkID09PSBkcmlsbC5pZCkge1xuICAgICAgICAgIHJlb212ZUluZGV4ID0gaW5kZXg7XG4gICAgICAgIH1cbiAgICAgIHJldHVybiBpdGVtLmlkID09PSBkcmlsbC5pZDtcbiAgICB9KTtcbiAgICBpZiAoIGNoZWNrZWQgKSB7XG4gICAgICBwbGFuLmRlc2NyaXB0aW9uWzBdLnNwbGljZShyZW9tdmVJbmRleCwgMSk7XG4gICAgICBjb25zb2xlLmxvZygncmVtb3ZlJywgcGxhbik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHBsYW4uZGVzY3JpcHRpb25bMF0ucHVzaChkcmlsbCk7XG4gICAgICBjb25zb2xlLmxvZygnYWRkJywgcGxhbik7XG4gICAgfVxuICAgICAgdGhpcy5wcmFjdGljZVNlcnZpY2UucHV0UmVxdWVzdChgYXBpL3NhdmVwbGFucy8ke3BsYW4uaWR9YCwgcGxhbikuc3Vic2NyaWJlKFxuICAgICAgICAocmVzOiBhbnkpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnYWRkZWQnLCByZXMpO1xuICAgICAgICB9LFxuICAgICAgICBlcnJvcjEgPT4ge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ2VycicsIGVycm9yMSk7XG4gICAgICAgIH1cbiAgICAgICk7XG4gICAgfVxufVxuIl19