/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @type {?} */
const headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
export class PracticePlannerService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
    }
    /**
     * @param {?} url
     * @return {?}
     */
    getRequest(url) {
        return this.http.get(`${url}`, { headers: headers });
    }
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    postRequest(url, data) {
        return this.http.post(`${url}`, data, { headers: headers });
    }
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    putRequest(url, data) {
        return this.http.put(`${url}`, data, { headers: headers });
    }
    /**
     * @param {?} url
     * @param {?} data
     * @return {?}
     */
    patchRequest(url, data) {
        return this.http.patch(`${url}`, data, { headers: headers });
    }
    /**
     * @param {?} url
     * @return {?}
     */
    removeRequest(url) {
        return this.http.delete(`${url}`, { headers: headers });
    }
}
PracticePlannerService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PracticePlannerService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ PracticePlannerService.ngInjectableDef = i0.defineInjectable({ factory: function PracticePlannerService_Factory() { return new PracticePlannerService(i0.inject(i1.HttpClient)); }, token: PracticePlannerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PracticePlannerService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJhY3RpY2UtcGxhbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZHJpbGwvIiwic291cmNlcyI6WyJzcmMvYXBwL2RyaWxscy9wcmFjdGljZS1wbGFubmVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMvRCxPQUFPLHVCQUF1QixDQUFDOzs7O01BRXpCLE9BQU8sR0FBRyxJQUFJLFdBQVcsRUFBRTtBQUNqQyxPQUFPLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0FBS25ELE1BQU0sT0FBTyxzQkFBc0I7Ozs7SUFFakMsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUFJLENBQUM7Ozs7O0lBRXpDLFVBQVUsQ0FBQyxHQUFHO1FBQ1osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Ozs7O0lBRUQsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJO1FBQ25CLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7Ozs7SUFFRCxVQUFVLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQUVELFlBQVksQ0FBQyxHQUFHLEVBQUUsSUFBSTtRQUNwQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDL0QsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsR0FBRztRQUNmLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsRUFBRyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7OztZQXpCRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFSUSxVQUFVOzs7Ozs7OztJQVdMLHNDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0ICdyeGpzL2FkZC9vcGVyYXRvci9tYXAnO1xuXG5jb25zdCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XG5oZWFkZXJzLmFwcGVuZCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUHJhY3RpY2VQbGFubmVyU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cblxuICBnZXRSZXF1ZXN0KHVybCkge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3VybH1gLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG4gIH1cblxuICBwb3N0UmVxdWVzdCh1cmwsIGRhdGEpIHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QoYCR7dXJsfWAsIGRhdGEsIHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgfVxuXG4gIHB1dFJlcXVlc3QodXJsLCBkYXRhKSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQoYCR7dXJsfWAsIGRhdGEsIHsgaGVhZGVyczogaGVhZGVycyB9KTtcbiAgfVxuXG4gIHBhdGNoUmVxdWVzdCh1cmwsIGRhdGEpIHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBhdGNoKGAke3VybH1gLCBkYXRhLCB7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG4gIH1cblxuICByZW1vdmVSZXF1ZXN0KHVybCkge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlKGAke3VybH1gLCAgeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xuICB9XG5cbn1cbiJdfQ==