(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/forms'), require('ng2-dropdown'), require('@angular/common/http'), require('rxjs/add/operator/map'), require('@angular/core'), require('@angular/router'), require('@angular/platform-browser'), require('ngx-smart-modal'), require('@angular/common'), require('rxjs/add/operator/filter')) :
    typeof define === 'function' && define.amd ? define('drill', ['exports', '@angular/forms', 'ng2-dropdown', '@angular/common/http', 'rxjs/add/operator/map', '@angular/core', '@angular/router', '@angular/platform-browser', 'ngx-smart-modal', '@angular/common', 'rxjs/add/operator/filter'], factory) :
    (factory((global.drill = {}),global.ng.forms,global.ng2Dropdown,global.ng.common.http,global.rxjs['add/operator/map'],global.ng.core,global.ng.router,global.ng.platformBrowser,global.ngxSmartModal,global.ng.common));
}(this, (function (exports,forms,ng2Dropdown,i1,map,i0,router,platformBrowser,ngxSmartModal,common) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var headers = new i1.HttpHeaders();
    headers.append('Content-Type', 'application/json');
    var PracticePlannerService = /** @class */ (function () {
        function PracticePlannerService(http) {
            this.http = http;
        }
        /**
         * @param {?} url
         * @return {?}
         */
        PracticePlannerService.prototype.getRequest = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                return this.http.get("" + url, { headers: headers });
            };
        /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
        PracticePlannerService.prototype.postRequest = /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
            function (url, data) {
                return this.http.post("" + url, data, { headers: headers });
            };
        /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
        PracticePlannerService.prototype.putRequest = /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
            function (url, data) {
                return this.http.put("" + url, data, { headers: headers });
            };
        /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
        PracticePlannerService.prototype.patchRequest = /**
         * @param {?} url
         * @param {?} data
         * @return {?}
         */
            function (url, data) {
                return this.http.patch("" + url, data, { headers: headers });
            };
        /**
         * @param {?} url
         * @return {?}
         */
        PracticePlannerService.prototype.removeRequest = /**
         * @param {?} url
         * @return {?}
         */
            function (url) {
                return this.http.delete("" + url, { headers: headers });
            };
        PracticePlannerService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        PracticePlannerService.ctorParameters = function () {
            return [
                { type: i1.HttpClient }
            ];
        };
        /** @nocollapse */ PracticePlannerService.ngInjectableDef = i0.defineInjectable({ factory: function PracticePlannerService_Factory() { return new PracticePlannerService(i0.inject(i1.HttpClient)); }, token: PracticePlannerService, providedIn: "root" });
        return PracticePlannerService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DrillsComponent = /** @class */ (function () {
        function DrillsComponent(practiceService, router$$1, ngxSmartModalService) {
            this.practiceService = practiceService;
            this.router = router$$1;
            this.ngxSmartModalService = ngxSmartModalService;
        }
        /**
         * @return {?}
         */
        DrillsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                console.log('-----', this.data);
            };
        /**
         * @param {?} drill
         * @param {?} index
         * @return {?}
         */
        DrillsComponent.prototype.detailClicked = /**
         * @param {?} drill
         * @param {?} index
         * @return {?}
         */
            function (drill, index) {
                this.router.navigate(['drills/detail'], { queryParams: { id: drill.id, i: index } });
            };
        /**
         * @return {?}
         */
        DrillsComponent.prototype.createCategoty = /**
         * @return {?}
         */
            function () {
                this.categoryTitle = '';
                this.actionType = 'submitCreate';
                this.ngxSmartModalService.getModal('createModal').open();
            };
        /**
         * @param {?} category
         * @param {?} i
         * @return {?}
         */
        DrillsComponent.prototype.edit = /**
         * @param {?} category
         * @param {?} i
         * @return {?}
         */
            function (category, i) {
                this.selectedIndex = i;
                this.actionType = 'submitEdit';
                this.categoryTitle = category.title;
                this.ngxSmartModalService.getModal('createModal').open();
            };
        /**
         * @param {?} data
         * @return {?}
         */
        DrillsComponent.prototype.submitEdit = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                var _this = this;
                this.practiceService.patchRequest("api/practicePlans/" + this.data[this.selectedIndex].id, data).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.data[_this.selectedIndex] = res;
                    _this.categoryTitle = '';
                    _this.ngxSmartModalService.getModal('createModal').close();
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.error('err', error1);
                }));
            };
        /**
         * @param {?} data
         * @return {?}
         */
        DrillsComponent.prototype.submitCreate = /**
         * @param {?} data
         * @return {?}
         */
            function (data) {
                var _this = this;
                data['items'] = [];
                this.practiceService.postRequest('api/practicePlans', data).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    console.log('res', res);
                    _this.data.push(res);
                    _this.categoryTitle = '';
                    _this.ngxSmartModalService.getModal('createModal').close();
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.error('err', error1);
                }));
            };
        /**
         * @return {?}
         */
        DrillsComponent.prototype.submit = /**
         * @return {?}
         */
            function () {
                console.log('title', this.categoryTitle);
                /** @type {?} */
                var data = {
                    title: this.categoryTitle
                };
                this[this.actionType](data);
            };
        /**
         * @param {?} i
         * @return {?}
         */
        DrillsComponent.prototype.remove = /**
         * @param {?} i
         * @return {?}
         */
            function (i) {
                this.selectedIndex = i;
                this.ngxSmartModalService.getModal('removeModal').open();
            };
        /**
         * @return {?}
         */
        DrillsComponent.prototype.submitRemove = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.practiceService.removeRequest("api/practicePlans/" + this.data[this.selectedIndex].id).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.data.splice(_this.selectedIndex, 1);
                    _this.ngxSmartModalService.getModal('removeModal').close();
                    _this.selectedIndex = NaN;
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.error('err', error1);
                }));
            };
        DrillsComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-drills',
                        template: "<div *ngIf=\"data\">\n  <div class=\"add-btn btn btn-main mb-15\" (click)=\"createCategoty()\"> Add Category </div>\n\n  <div *ngFor=\"let categoryDrill of data; let i = index\" class=\"categories mb-15\">\n    <p (click)=\"detailClicked(categoryDrill, i)\">\n      {{categoryDrill.title}}\n    </p>\n    <div class=\"actions\">\n      <span class=\"edit glyphicon glyphicon-pencil\" title=\"Edit\" (click)=\"edit(categoryDrill, i)\"></span>\n      <span class=\"delete glyphicon glyphicon-trash\" title=\"Remove\" (click)=\"remove(i)\"></span>\n    </div>\n  </div>\n</div>\n\n<ngx-smart-modal #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal #createModal identifier=\"createModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitCreate'\">Create New Category</p>\n    <p class=\"modal-title text-center\" *ngIf=\"actionType === 'submitEdit'\">Edit Category</p>\n    <p>\n      <input type=\"text\" placeholder=\"Category Title\" [(ngModel)]=\"categoryTitle\" class=\"form-control\">\n    </p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"createModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submit()\">Create</button>\n    </div>\n\n  </div>\n</ngx-smart-modal>\n",
                        styles: [".categories{position:relative;border-radius:3px;color:#000;background:#fff;padding:5px;border:1px solid #ccc}.categories .actions{position:absolute;top:0;right:0;padding:5px}.categories .actions span{padding:0 5px;cursor:pointer}.categories p{margin-bottom:0}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}"]
                    }] }
        ];
        /** @nocollapse */
        DrillsComponent.ctorParameters = function () {
            return [
                { type: PracticePlannerService },
                { type: router.Router },
                { type: ngxSmartModal.NgxSmartModalService }
            ];
        };
        DrillsComponent.propDecorators = {
            data: [{ type: i0.Input }]
        };
        return DrillsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DrillsDetailsComponent = /** @class */ (function () {
        function DrillsDetailsComponent(practiceService, route, router$$1, datePipe, sanitizer, ngxSmartModalService) {
            this.practiceService = practiceService;
            this.route = route;
            this.router = router$$1;
            this.datePipe = datePipe;
            this.sanitizer = sanitizer;
            this.ngxSmartModalService = ngxSmartModalService;
        }
        /**
         * @return {?}
         */
        DrillsDetailsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var drillIndex;
                this.route.queryParams
                    .filter(( /**
             * @param {?} params
             * @return {?}
             */function (params) { return params.id; }))
                    .subscribe(( /**
             * @param {?} params
             * @return {?}
             */function (params) {
                    drillIndex = params.id;
                }));
                this.practiceService.getRequest("api/practicePlans/" + drillIndex).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.drill = res;
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.log('errr', error1);
                }));
                this.practiceService.getRequest('api/saveplans').subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.plans = res;
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.log('error', error1);
                }));
            };
        /**
         * @return {?}
         */
        DrillsDetailsComponent.prototype.back = /**
         * @return {?}
         */
            function () {
                this.router.navigate(['drills']);
            };
        /**
         * @param {?} index
         * @return {?}
         */
        DrillsDetailsComponent.prototype.remove = /**
         * @param {?} index
         * @return {?}
         */
            function (index) {
                this.ngxSmartModalService.getModal('removeModal').open();
                this.selectedIndex = index;
            };
        /**
         * @return {?}
         */
        DrillsDetailsComponent.prototype.submitRemove = /**
         * @return {?}
         */
            function () {
                var _this = this;
                this.drill.items.splice(this.selectedIndex, 1);
                this.practiceService.putRequest("api/practicePlans/" + this.drill.id, this.drill).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.ngxSmartModalService.getModal('removeModal').close();
                    _this.selectedIndex = NaN;
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.log('remove error', error1);
                }));
            };
        /**
         * @param {?} drill
         * @param {?} i
         * @return {?}
         */
        DrillsDetailsComponent.prototype.edit = /**
         * @param {?} drill
         * @param {?} i
         * @return {?}
         */
            function (drill, i) {
                this.selectedDrill = drill;
                this.selectedIndex = i;
                this.ngxSmartModalService.getModal('editModal').open();
            };
        /**
         * @return {?}
         */
        DrillsDetailsComponent.prototype.submitEdit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                if (this.selectedIndex || !isNaN(this.selectedIndex)) {
                    this.drill.items[this.selectedIndex] = this.selectedDrill;
                }
                else {
                    this.drill.items.push(this.selectedDrill);
                }
                this.practiceService.putRequest("api/practicePlans/" + this.drill.id, this.drill).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    _this.ngxSmartModalService.getModal('editModal').close();
                    _this.selectedIndex = NaN;
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.error('edit error', error1);
                }));
            };
        /**
         * @return {?}
         */
        DrillsDetailsComponent.prototype.create = /**
         * @return {?}
         */
            function () {
                this.selectedDrill = {
                    title: '',
                    descriptionA: '',
                    descriptionB: '',
                    descriptionC: '',
                    descriptionD: '',
                    descriptionE: '',
                    descriptionF: '',
                    videoURL: '',
                    id: this.datePipe.transform(new Date(), 'yyyy MM dd HH MM SS MM').replace(/\s/g, '')
                };
                this.ngxSmartModalService.getModal('editModal').open();
            };
        /**
         * @param {?} plan
         * @param {?} drill
         * @return {?}
         */
        DrillsDetailsComponent.prototype.check = /**
         * @param {?} plan
         * @param {?} drill
         * @return {?}
         */
            function (plan, drill) {
                /** @type {?} */
                var checked = plan.description[0].find(( /**
                 * @param {?} item
                 * @return {?}
                 */function (item) {
                    return item.id === drill.id;
                }));
                return checked ? true : false;
            };
        /**
         * @param {?} plan
         * @param {?} drill
         * @return {?}
         */
        DrillsDetailsComponent.prototype.addToPlan = /**
         * @param {?} plan
         * @param {?} drill
         * @return {?}
         */
            function (plan, drill) {
                /** @type {?} */
                var reomveIndex;
                /** @type {?} */
                var checked = plan.description[0].find(( /**
                 * @param {?} item
                 * @param {?} index
                 * @return {?}
                 */function (item, index) {
                    if (item.id === drill.id) {
                        reomveIndex = index;
                    }
                    return item.id === drill.id;
                }));
                if (checked) {
                    plan.description[0].splice(reomveIndex, 1);
                    console.log('remove', plan);
                }
                else {
                    plan.description[0].push(drill);
                    console.log('add', plan);
                }
                this.practiceService.putRequest("api/saveplans/" + plan.id, plan).subscribe(( /**
                 * @param {?} res
                 * @return {?}
                 */function (res) {
                    console.log('added', res);
                }), ( /**
                 * @param {?} error1
                 * @return {?}
                 */function (error1) {
                    console.error('err', error1);
                }));
            };
        DrillsDetailsComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'app-drills-details',
                        template: "<div *ngIf=\"drill\" class=\"\">\n  <div>\n    <div class=\"back\">\n      <button (click)=\"back()\" type=\"button\" class=\"btn btn-primary\">\n        <img src=\"../../../assets/images/60577.png\" alt=\"\">\n        Back\n      </button>\n      <button (click)=\"create()\" class=\"btn btn-main\">Create Drill</button>\n    </div>\n  </div>\n  <h2 class=\"text-center main-title\">{{drill.title}}</h2>\n  <div *ngFor=\"let item of drill.items; let i = index\" class=\"drill row\">\n      <div class=\"drill-head-section flexbox\">\n        <h3 class=\"drill-heading m-0\">{{item.title}}</h3>\n        <div class=\"actions\">\n          <span class=\"glyphicon glyphicon-pencil btn\" title=\"Edit\" (click)=\"edit(item, i)\"></span>\n          <span class=\"glyphicon glyphicon-trash btn\" title=\"Remove\" (click)=\"remove(i)\"></span>\n          <span class=\"dropdown\" dropdown>\n            <button class=\"btn btn-link\" dropdown-open>Add to plan</button>\n            <ul class=\"dropdown-menu\" dropdown-not-closable-zone>\n              <li *ngFor=\"let plan of plans\">\n                <input type=\"checkbox\" [checked]=\"check(plan, item)\" (change)=\"addToPlan(plan, item)\">\n                <label>\n                  {{plan.description[1].name}}\n                </label>\n              </li>\n            </ul>\n          </span>\n        </div>\n      </div>\n      <div class=\"drill-content\">\n        <p *ngIf=\"item.descriptionA\">{{item.descriptionA}}</p>\n        <p *ngIf=\"item.descriptionB\">{{item.descriptionB}}</p>\n        <p *ngIf=\"item.descriptionC\">{{item.descriptionC}}</p>\n        <p *ngIf=\"item.descriptionD\">{{item.descriptionD}}</p>\n        <p *ngIf=\"item.descriptionE\">{{item.descriptionE}}</p>\n        <p *ngIf=\"item.descriptionF\">{{item.descriptionF}}</p>\n        <p class=\"mb-0\">\n          <iframe width=\"520\" height=\"310\" class=\"e2e-iframe-trusted-src\" [src]=\"sanitizer.bypassSecurityTrustResourceUrl(item.videoURL)\" frameborder=\"0\" allowfullscreen></iframe>\n        </p>\n      </div>\n  </div>\n</div>\n<ngx-smart-modal  *ngIf=\"drill\" #removeModal identifier=\"removeModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Are you sure you want to delete the drill?</p>\n    <div class=\"text-center\">\n      <button class=\"btn btn-danger\" (click)=\"removeModal.close()\">Cancel</button>\n      <button class=\"btn btn-primary\" (click)=\"submitRemove()\">Remove</button>\n    </div>\n  </div>\n</ngx-smart-modal>\n\n<ngx-smart-modal  *ngIf=\"drill\" #editModal identifier=\"editModal\">\n  <div class=\"modal-content-inner\">\n    <p class=\"modal-title text-center\">Edit Drill</p>\n   <div *ngIf=\"selectedDrill\">\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Title</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.title\" name=\"\" id=\"title\" cols=\"30\" rows=\"3\">{{selectedDrill.title}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description A</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionA\" name=\"\" id=\"descriptionA\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionA}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description B</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionB\" name=\"\" id=\"descriptionB\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionB}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description C</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionC\" name=\"\" id=\"descriptionC\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionC}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description D</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionD\" name=\"\" id=\"descriptionD\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionD}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description E</div>\n           <textarea class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionE\" name=\"\" id=\"descriptionE\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionE}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Description F</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.descriptionF\" name=\"\" id=\"descriptionF\" cols=\"30\" rows=\"3\">{{selectedDrill.descriptionF}}</textarea>\n         </div>\n       </div>\n       <div class=\"form-group\">\n         <div class=\"input-group\">\n           <div class=\"input-group-addon\">Video Url</div>\n           <textarea  class=\"form-control\" [(ngModel)]=\"selectedDrill.videoURL\" name=\"\" id=\"videoURL\" cols=\"30\" rows=\"3\">{{selectedDrill.videoURL}}</textarea>\n         </div>\n       </div>\n   </div>\n\n  <div class=\"text-center\">\n    <button class=\"btn btn-danger\" (click)=\"editModal.close()\">Cancel</button>\n    <button class=\"btn btn-primary\" (click)=\"submitEdit()\">Save</button>\n  </div>\n  </div>\n</ngx-smart-modal>\n",
                        styles: [".drill{margin:25px auto;background:#fff}.drill .actions span{display:inline-block;margin-left:10px;padding:0}.actions .btn:hover{color:#fff}.actions .btn:active{box-shadow:none}.actions .btn:focus{color:#fff;outline:0}.btn-link{color:#fff;text-decoration:underline}.back img{height:17px}.back .btn{margin-right:10px}.modal-content-inner{padding:20px 0 35px}.modal-content-inner .modal-title{font-size:18px;margin-bottom:15px}.modal-content-inner input{margin-bottom:20px}.modal-content-inner .btn{width:100px;margin-right:10px}.modal-content-inner .form-control{resize:vertical}.main-title{color:#035596}.glyphicon{top:0}.dropdown-menu{top:40px;right:0;left:auto;padding:10px 5px;margin:5px 0}.dropdown-menu li{display:flex;align-items:center;margin-bottom:5px}.dropdown-menu li input{margin:0 5px 0 0}.dropdown-menu li label{margin-bottom:0;color:#000;font-weight:500}.input-group-addon{width:110px}.btn-primary{font-weight:600;text-transform:uppercase;border-color:transparent;background-color:#0070c9}.btn-primary:hover{background-color:#035596}.drill-head-section{padding:15px;background:#0070c9;color:#fff;border-top-left-radius:4px;border-top-right-radius:4px;justify-content:space-between}.drill-heading{color:#fff;font-size:18px;line-height:unset;padding:4px 0}.drill-content{padding:10px 20px 30px}.mb-0{margin-bottom:0}iframe{width:100%}"]
                    }] }
        ];
        /** @nocollapse */
        DrillsDetailsComponent.ctorParameters = function () {
            return [
                { type: PracticePlannerService },
                { type: router.ActivatedRoute },
                { type: router.Router },
                { type: common.DatePipe },
                { type: platformBrowser.DomSanitizer },
                { type: ngxSmartModal.NgxSmartModalService }
            ];
        };
        DrillsDetailsComponent.propDecorators = {
            data: [{ type: i0.Input }]
        };
        return DrillsDetailsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DrillsModule = /** @class */ (function () {
        function DrillsModule() {
        }
        DrillsModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [DrillsComponent, DrillsDetailsComponent],
                        imports: [
                            common.CommonModule,
                            ngxSmartModal.NgxSmartModalModule.forRoot(),
                            forms.FormsModule,
                            ng2Dropdown.DropdownModule
                        ],
                        providers: [
                            common.DatePipe,
                            PracticePlannerService
                        ],
                        exports: [
                            DrillsComponent,
                            DrillsDetailsComponent
                        ]
                    },] }
        ];
        return DrillsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.DrillsModule = DrillsModule;
    exports.ɵc = DrillsDetailsComponent;
    exports.ɵa = DrillsComponent;
    exports.ɵb = PracticePlannerService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=drill.umd.js.map