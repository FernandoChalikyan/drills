import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
export declare class PracticePlannerService {
    private http;
    constructor(http: HttpClient);
    getRequest(url: any): import("rxjs/internal/Observable").Observable<Object>;
    postRequest(url: any, data: any): import("rxjs/internal/Observable").Observable<Object>;
    putRequest(url: any, data: any): import("rxjs/internal/Observable").Observable<Object>;
    patchRequest(url: any, data: any): import("rxjs/internal/Observable").Observable<Object>;
    removeRequest(url: any): import("rxjs/internal/Observable").Observable<Object>;
}
