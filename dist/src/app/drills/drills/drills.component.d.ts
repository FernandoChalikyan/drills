import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PracticePlannerService } from '../practice-planner.service';
export declare class DrillsComponent implements OnInit {
    private practiceService;
    private router;
    ngxSmartModalService: NgxSmartModalService;
    data: any;
    categoryTitle: string;
    selectedIndex: number;
    actionType: string;
    constructor(practiceService: PracticePlannerService, router: Router, ngxSmartModalService: NgxSmartModalService);
    ngOnInit(): void;
    detailClicked(drill: any, index: any): void;
    createCategoty(): void;
    edit(category: any, i: any): void;
    submitEdit(data: any): void;
    submitCreate(data: any): void;
    submit(): void;
    remove(i: any): void;
    submitRemove(): void;
}
