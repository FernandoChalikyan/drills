/**
 * Generated bundle index. Do not edit.
 */
export * from './public_api';
export { DrillsDetailsComponent as ɵc } from './src/app/drills/drills-details/drills-details.component';
export { DrillsComponent as ɵa } from './src/app/drills/drills/drills.component';
export { PracticePlannerService as ɵb } from './src/app/drills/practice-planner.service';
