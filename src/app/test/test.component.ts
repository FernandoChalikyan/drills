import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  public Data = [
    {
      "id": "5c592f35521d8d30aa81678e",
      "title": "Throwing Drills",
      "items": [
        {
          "title": "Throwing Progression For A Team",
          "descriptionA": "A good throwing progression is going to involve consistancy, simplicity, and teamwork.",
          "descriptionB": "Everytime you do a throwing progression make sure everyone is on the same page and do it as a team so kids get used to the same types of throwing techniques.",
          "descriptionC": "You can mix in certain drills from week to week but keep the same 3-5 drills all the time. For more ideas browse our throwing drill library!",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/232722024"
        },
        {
          "title": "Wrist Flips",
          "descriptionA": "Using 4-seam grip and about 10-12 feet apart, aim throwing elbow at partner at shoulder height and extend arm to flip ball keeping fingers on top of ball to create backspin.",
          "descriptionB": "End with fingers pointing down and arm fully extended pointing at partner (can be done on one knee as well).",
          "descriptionC": "For added form, put black tape around ball perpendicular to the 4-seams so when the ball is flipped properly, the tape rotates in a straight line up and down.",
          "descriptionD": "Make sure the throw is sharp and they don't arc the ball across.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/cjf1iEH3D3c\\"
        },
        {
          "title": "Point And Whip",
          "descriptionA": "The point and whip is a great way to get an player using their limbs and getting a good scap load before the throw.",
          "descriptionB": "It also involves rhythm and tempo that can be a little more fun for younger athletes.",
          "descriptionC": "It makes them point and focus on their target and then generating a nice arm circle and whip as they throw!",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180683350"
        },
        {
          "title": "One Knee Standups",
          "descriptionA": "Thrower starts on with throwing side knee on the ground.",
          "descriptionB": "Player follows through and stands up.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Ten Toes",
          "descriptionA": "This is a drill every baseball player should know and use!",
          "descriptionB": "It involves a stable base as you point both feet at your target and allowing your torso along with your hands to spread and get a scap load to arm circle to then follow through to your partner!",
          "descriptionC": "This drill works on everything and is important when you want to breakdown the mechanics of the upper body.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Figure 8",
          "descriptionA": "This is just like the wide stance drill except you're going to make a figure 8 with yours hands to allow for the player to feel his body and create rhythm throughout the mechanics of the throw.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "One Knee Drill",
          "descriptionA": "With players on one knee about 15-20 feet apart, throwing side knee on ground and glove side knee up pointing toward partner, throw to each other. Start with ball in glove at chest.",
          "descriptionB": "Pay attention to hand break (hands should break palms down), arm angles (upper arms parallel to the ground, equal bend in the elbows, palms facing away from each other).",
          "descriptionC": "Pause at hand break to check form, then throw.",
          "descriptionD": "Glove position during throw (glove should stay near chest during throw).",
          "descriptionE": "Follow through (over glove side knee with throwing arm).",
          "descriptionF": "Ball with black tape also good addition to this drill.",
          "videoURL": "http://www.youtube.com/embed/yd7uqcCizyY\\"
        },
        {
          "title": "Playing Catch",
          "descriptionA": "Key factors are point at your target, step at your target, look at your target, and finish at your target. Meanwhile get efficient separation with your hands, have a correct grip, and stay balanced throughout your delivery.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612410"
        },
        {
          "title": "Feet Planted Drill",
          "descriptionA": "With both feet planted shoulder width apart and pointing toward partner about 20 feet apart, rotate and throw to partner. Start with ball in glove at chest.",
          "descriptionB": "Pay attention to starting in an athletic position (knees slightly bent), hand break (hands should break palms down), arm angles (upper arms parallel to the ground, equal bend in the elbows, palms facing away from each other) and upper body rotation (front shoulder should point at partner).",
          "descriptionC": "Pause at hand break to check form, then throw.",
          "descriptionD": "Glove position during throw (glove should stay near chest during throw).",
          "descriptionE": "Follow through (throwing arm should end over glove side knee).",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/rkzJUFh8Z3o\\"
        },
        {
          "title": "Sideways To Target",
          "descriptionA": "Standing sideways to your partner with feet spread further than shoulder width about 20-25 feet apart, throw to partner. Start with ball in glove at chest.",
          "descriptionB": "Pay attention to starting in an athletic position (knees slightly bent), hand break (hands should break palms down) and arm angles (upper arms parallel to the ground, equal bend in the elbows, palms facing away from each other).",
          "descriptionC": "Pause at hand break to check form, then throw.",
          "descriptionD": "Glove position during throw (glove should stay near chest during throw).",
          "descriptionE": "Follow through (throwing arm should end over glove side knee).",
          "descriptionF": "Can be done keeping back foot planted or regular follow through with back leg.",
          "videoURL": "http://www.youtube.com/embed/a8fZ-m6X1tg\\"
        },
        {
          "title": "Crossfire",
          "descriptionA": "This drill is very similar to the figure 8 drill except we will not have our hands together but we will cross them in front and completelt separate when your weight is back. Its key factors are to gain rhythm and help create arm separation on the throw.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Crossfire With Step",
          "descriptionA": "Do the same thing as the traditional crossfire drill except now we are gaining momentum and moving our feet during the throw.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Step And Throw",
          "descriptionA": "Start with feet planted, facing partner and ball in glove at chest.",
          "descriptionB": "Thrower takes a step forward with throwing side leg and throws to partner.",
          "descriptionC": "In addition to all above fundamentals (hand break, arm angle, glove position during throw and follow through), now coach is paying attention to thrower getting sideways to his partner and the direction of his stepping leg.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/LlRee_5RaiA\\"
        },
        {
          "title": "Step Behinds",
          "descriptionA": "This drill involves a more classic throw while teaching the athlete to throw with more power and how to gain momemtum on longer throws.",
          "descriptionB": "It is because the idea is to hold your body after the throw to see if you followed through and were balanced.",
          "descriptionC": "Just like the drill implies we step behind our front foot to gain ground and create a harder throw.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Through And Hold Through",
          "descriptionA": "This drill can be added to almost any throwing drill!",
          "descriptionB": "Have your player throw the ball and make sure his head and arm are parallel to the ground.",
          "descriptionC": "Maybe even make them pick up some grass after the throw as a reminder to follow thru.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Side To Side Drill",
          "descriptionA": "Very similar to the figure 8 drill but with this we are rocking side to side much like any infielder may do while recieving a throw or fielding a groundball.",
          "descriptionB": "It involves balance and rhythm.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Shuffle And Throw",
          "descriptionA": "With partners further apart, have players start sideways to their partner and take two shuffle steps and throw to partner.",
          "descriptionB": "Pay attention to hand break, arm angles, body position after step, glove position during throw, follow through and direction of stepping leg.",
          "descriptionC": "Throw should be firm.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/-PGbAQb119E\\"
        },
        {
          "title": "Crow Hop And Throw",
          "descriptionA": "While stretched out to the peak of your throwing distance, have players take a more explosive first step with throwing side leg forward or crow hop and throw to partner.",
          "descriptionB": "Pay attention to hand break, arm angles, body position after hop, glove position during throw, follow through and direction of stepping leg.",
          "descriptionC": "The use of a small object (or mini hurdle) that the kids have to jump over can help kids get the feel of it.",
          "descriptionD": "Throw should be firm.",
          "descriptionE": "If done indoors with limited space, drill can be done throwing into a net. Pick a target in the netting to simulate a line drive throw from the outfield.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/ciAGgb6hOos\\"
        },
        {
          "title": "Crow Hop Over",
          "descriptionA": "This is similar to the traditional crow hop except now, you're going to put an object there (bat, hat, ect.) that the athlete will crow hop over to teach utilizing the power of your legs on a throw.",
          "descriptionB": "It also involves foot coordination and balance to better be accurate on your throws.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Quick Hands And Quick Feet",
          "descriptionA": "Partners are throwing to each other and moving their feet quickly to the ball and getting the ball out of their glove and in throwing position fast.",
          "descriptionB": "Receiving partner should be in an athletic starting position, have his hands up at chest level, and have his elbows down in a relaxed position.",
          "descriptionC": "Use your feet to move to the ball and catch the ball in front of your body with arms nearly fully extended.",
          "descriptionD": "Catch the ball in the palm of your glove and secure with throwing hand.",
          "descriptionE": "In the same motion with throwing hand already on ball, transfer ball to throwing hand and get sideways to your partner in throwing position as fast as possible. Pause, check form, then throw.",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612415\\"
        },
        {
          "title": "Quick Hands Quick Feet",
          "descriptionA": "Partners are throwing to each other and moving their feet quickly to the ball and getting the ball out of their glove and in throwing position fast.",
          "descriptionB": "Receiving partner should be in an athletic starting position, have his hands up at chest level, and have his elbows down in a relaxed position.",
          "descriptionC": "Use your feet to move to the ball and catch the ball in front of your body with arms nearly fully extended.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/mDP81N3QlQs\\"
        },
        {
          "title": "Quick Hands Closed Glove",
          "descriptionA": "Partners are throwing to each other and moving their feet quickly to the ball and catching the ball on the outside of their glove and in throwing position fast.",
          "descriptionB": "Receiving partner should be in an athletic starting position, have his hands up at chest level, and have his elbows down in a relaxed position.",
          "descriptionC": "Use your feet to move to the ball and catch the ball in front of your body with arms nearly fully extended.",
          "descriptionD": "Catch the ball against the outside of your glove attempting to barely let the glove touch the ball and directing the ball into your throwing hand.",
          "descriptionE": "This is an advanced drill for experience players only.",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Follow Through",
          "descriptionA": "Any player that wants to put less stress on his arm needs to do this skill.",
          "descriptionB": "Following through not only helps you stay true on your throws but helps decrease the stress on your elbow and shoulder by sending the energy throughout the rest of your body.",
          "descriptionC": "Anyone who wants to have a healthy arm, throw harder, and more accurate will always work on a good follow through!",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Bare Hands Wiffle Catch",
          "descriptionA": "For the beginner player, playing catch with wiffle balls bare handed.",
          "descriptionB": "Variation: Have them use one hand only. Really gives them a sense of when they need to close their hand to catch.",
          "descriptionC": "Add a second ball for more difficulty and fun.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612413\\"
        },
        {
          "title": "Single-Hand Catching",
          "descriptionA": "Instead of using two hands to catch the ball, have the players use only one hand.",
          "descriptionB": "Have the players concentrate on securing the ball in the glove without the benefit of the throwing hand trapping the ball.",
          "descriptionC": "Also works well with a tennis ball as they are lighter and harder to secure.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612402\\"
        },
        {
          "title": "Running Throws To Set Target",
          "descriptionA": "With ball in hand, run to your left and throw to a stationary target (or player) to the right.",
          "descriptionB": "With ball in hand, run to your right and throw to a stationary target (or player) to the left.",
          "descriptionC": "Can do this drill while throwing on the run or planting feet, getting square to the target, then throwing.",
          "descriptionD": "For added difficulty, have the players run without the ball and throw them the ball on the run, then they have to throw to the target.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Throws To Moving Target",
          "descriptionA": "Have stationary player throw to moving target.",
          "descriptionB": "Have player or coach with ball (quarterback) and another player (receiver) about 15-20 feet to his left.",
          "descriptionC": "Receiver runs a post pattern (straight ahead for 10 yards, the breaks slightly to his right). Quarterback has to hit him in stride, receiver has to catch the ball.",
          "descriptionD": "Switch and have the receiver stand to the right of the quarterback.",
          "descriptionE": "Can also line the receivers up across from the quarterback and have them run right/left.",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Partner Running Throws",
          "descriptionA": "Have players line up about 30 feet apart (or age appropriate distance).",
          "descriptionB": "Both start jogging in the same direction at the same time, stay 30 feet apart and throw to each other while jogging the length of the baseline.",
          "descriptionC": "Switch directions and go the other way.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Speed Throwing",
          "descriptionA": "At a set, age appropriate distance, partners see how many times they can catch and throw to each other in 30 seconds.",
          "descriptionB": "For older players concentrate on moving feet before ball is in glove.",
          "descriptionC": "Most catches wins.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Two Ball Catch",
          "descriptionA": "Play catch with two balls, throwing one while the other is in your glove.",
          "descriptionB": "After throwing the ball, take the ball out of your glove and keep it in your throeing hand until you catch the next ball.",
          "descriptionC": "This drill will help eliminate sloppy glove placement and encourage holding the glove close to your body while throwing.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612416\\"
        },
        {
          "title": "Long Toss",
          "descriptionA": "Players gradually extending the distance between throws until they are throwing as far as they can and as fluidly as possible. Take a step back after every throw.",
          "descriptionB": "When they get to their max distance, gradually move closer and now are throwing line drives to their partner as hard as they can and as fluidly as possible.",
          "descriptionC": "If space is an issue indoors, it can be done throwing into a net.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612419\\"
        }
      ]
    },
    {
      "id": "5c592f7e521d8d30aa81678f",
      "title": "Infield Drills",
      "items": [
        {
          "title": "Triangle Drill",
          "descriptionA": "This is an easy drill to get the concept of how to field a ground ball.",
          "descriptionB": "Quite simply you draw a triangle in the dirt and then have your players put two feet on the edges and the glove at the tip of the triangle.",
          "descriptionC": "This represents good form when fielding a ground ball.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Fast Five",
          "descriptionA": "This is something you can use with the triangle drill.",
          "descriptionB": "Have them stand on there triangle of in good groundball poisition and have your athlete field 5 balls in a row very fast practicing receiving and exchange throughout the fielding process.",
          "descriptionC": "It focuses on quick hands and good form.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180683373"
        },
        {
          "title": "Groundball Progression On Knees",
          "descriptionA": "This is a way of practicing recieving the ball without having to integrate standing or using your feet.",
          "descriptionB": "It better helps an athlete work on the receiving and eye hand coordination that is involved with grounders.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612259"
        },
        {
          "title": "Groundball Progression",
          "descriptionA": "Groundball progression always starts with receiving the ball with only working the hands and then gradually add the footwork.",
          "descriptionB": "I like to works just hands and fielding, then fielding while stationary, and then fielding while moving towards the ball!",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Stationary Grounders",
          "descriptionA": "This is going to help your athlete see his range of the mitt while not using the feet to move to the ball.",
          "descriptionB": "Work on 2 hands and getting low",
          "descriptionC": "Shift weight to where you field the ball",
          "descriptionD": "Exchange",
          "descriptionE": "Getting low and extending those hands to the ball will be the best practice!",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612260"
        },
        {
          "title": "Short Hops",
          "descriptionA": "Coach is throwing short hops to player to backhand, forehand and middle (can also be done during catch with players throwing to each other).",
          "descriptionB": "Short hop should be thrown so it bounces approximately 6-12 inches in front of fielding position of fielder.",
          "descriptionC": "How to go out and get a forehand short hop - With fingers pointing to the ground on glove hand with arm extended, place glove where ball will bounce up and push hand to ball, simultaneously close glove, and close wrist to chest.",
          "descriptionD": "How to catch a backhand short hop - Catching a backhand hop is all about timing and being able to keep your glove behind the ball for as long as possible.",
          "descriptionE": "Your glove action on a short hop should be down, out and up. That is, when you reach for the short hop, your glove hand extends down below the ball (right behind the path of the ball), goes out to meet the ball, and continues up past the path of the ball and up.",
          "descriptionF": "For short hops in the middle of your body, either technique may be used.\nConcentrate on good fielding position.\nShort hops should be used while also working on footwork but can be done stationary too.\nIt really involves getting low, using soft hands, and learing to react to balls at different speeds and hops.\n",
          "videoURL": "https://player.vimeo.com/video/180612403\nhttp://www.youtube.com/embed/AkmTIDpNWZo\\\n\nhttp://www.youtube.com/embed/Pu0BK9QDu5w\\"
        },
        {
          "title": "Short Hop Drill",
          "descriptionA": "Have a partner be 5 feet from each other in proper form",
          "descriptionB": "Throw ball in front of them",
          "descriptionC": "Have the partners go easier to harder and practice 2 hands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Fielding A Big Hop",
          "descriptionA": "Fielding a big hop is easy if it just pops up to you, but the trick is to have your athlete get the timing down of fielding and coming through the ball at about waist to chest high.",
          "descriptionB": "Never assume your going to get a easy bounce",
          "descriptionC": "Definitely practice on real grass or dirt because if you practice this inside you will always get the same hops but on the real field it's not as basic.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612436"
        },
        {
          "title": "Fielding A Stationary Ball In The Infield",
          "descriptionA": "A stationary ball in the field should almost always be picked up with the bare hand or throwing hand.",
          "descriptionB": "This is because the mitt isnt as a reliable source when the ball is just sitting there! It often falls out. Then you have to do an exchnage out of your mitt cleanly to make a throw, so by just picking it up barehand you eliminate a step and have a better chnace of picking it up the 1st time.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612434"
        },
        {
          "title": "Side Shuffle One Hops - Second Attempt",
          "descriptionA": "This is going to help a fielder with his range in the field.",
          "descriptionB": "Having a bigger range enables a player to make more of the plays that would otherwise be hits.",
          "descriptionC": "Practice going to the right and to the left and practice the angel to the ball to better position yourself to make the play.",
          "descriptionD": "A side shuffle helps you get the footwork down to properly receive the ball cleanly.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Throwing Position Shuffles",
          "descriptionA": "This type of shuffle is done after the player has cleanly received the ball and now wants to make the throw.",
          "descriptionB": "Shuffles help players be more accurate, throw harder, and shorten the distance on the throw.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Drop, Hop And Shuffle",
          "descriptionA": "This drill breaks down the dynamics of the throw after they have recived the ball.",
          "descriptionB": "You drop low while you receive the ball with your legs.",
          "descriptionC": "Hop sideways to your target, and then shuffle to your target to ensure a clean throw.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Self Hops - 1,2,3",
          "descriptionA": "1,2,3 is something a player can say in there head as they go through the steps of recieving the ball which is 1 ball, 2 step, 3 step throw",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Side To Sides",
          "descriptionA": "This is a quick drill that helps the player with his range",
          "descriptionB": "They need to be quick and low and practice good form",
          "descriptionC": "Often can be used with multiple reps at one time",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "1St Basemen Footwork",
          "descriptionA": "Practice going to the base and getting ready to recieve the throw",
          "descriptionB": "Righty's- right foot on",
          "descriptionC": "Lefty's- left foot on",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Quick Hands Quick Feet Drill",
          "descriptionA": "Can be done with ball or no ball",
          "descriptionB": "Keys- be quick, butt down, hands out, watch the ball all the way into the mitt, and practice exchange",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Bare Hand Ground Balls",
          "descriptionA": "Field with bare hands, can start with baseball tennis balls etc.",
          "descriptionB": "Works on soft hands and getting low to the ball",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612406"
        },
        {
          "title": "3 Ball Charge Drill And Throw",
          "descriptionA": "Place 3 balls in front of athlete at different distances",
          "descriptionB": "Have athlete field and throw the advance to next ball",
          "descriptionC": "Practice good form and being quick",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612271"
        },
        {
          "title": "Backhand Grounders",
          "descriptionA": "This is a more advanced skill",
          "descriptionB": "Have to utilize legs and get low",
          "descriptionC": "Work on stepping with both legs and field ball outside of legs",
          "descriptionD": "Have your mitt scoop through the ball not swipe at it",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612264"
        },
        {
          "title": "Backhand With Crossover Drill",
          "descriptionA": "Your foot will cross over your other and will do a lunge essentially",
          "descriptionB": "Have your mitt outside your leg and watch ball into mitt",
          "descriptionC": "Have a clear opening for the ball to enter the mitt",
          "descriptionD": "Get low",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612271"
        },
        {
          "title": "Glove Extension Drill",
          "descriptionA": "Place bat in front of grounder line.",
          "descriptionB": "Players should start toes next to the bat, fielding the ball from a stationary position arms extending to field the ball in front of the bat.",
          "descriptionC": "Add approaching the grounder with a righty stepping left then right to the bat and fielding the ball.",
          "descriptionD": "For younger kids you can draw a triangle in the dirt to show where their glove should be.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612268\\"
        },
        {
          "title": "Arm Extension Drill",
          "descriptionA": "Start with a glove in fornt of your body almost fully extended",
          "descriptionB": "Field the ball low and with two hands",
          "descriptionC": "Come through the ball cleanly",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612268"
        },
        {
          "title": "Arm Extension Drill With Slow Roller",
          "descriptionA": "Start with a glove in front of your body almost fully extended",
          "descriptionB": "Field the ball low and with two hands",
          "descriptionC": "Add footwork to the ball",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612266"
        },
        {
          "title": "Groundball Footwork",
          "descriptionA": "Get low and 2 hands",
          "descriptionB": "Footwork- right left down",
          "descriptionC": "Finish- right, left, throw to target",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612268"
        },
        {
          "title": "Glove Side Grounders",
          "descriptionA": "Work on watching the ball into your mitt and fielding it more behind you",
          "descriptionB": "Soft hands and glove comes to the ball",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612262"
        },
        {
          "title": "Grounders On Knees",
          "descriptionA": "Fielding the ball in front of your body",
          "descriptionB": "Arms extended- 2 hands",
          "descriptionC": "Balance",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612261"
        },
        {
          "title": "With Progression On Knees",
          "descriptionA": "Work in front of your body fielding",
          "descriptionB": "To the front side of your body- forhands",
          "descriptionC": "To the backside of your body- backhands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Ground Balls",
          "descriptionA": "For the younger levels, it is easiest to start rolling grounders to them first. As a coach, you have better control of the throw/roll and will be able to put the ball in the location you want easier.",
          "descriptionB": "A good fielding position consists of legs spread wider than shoulder width with toes pointing forward, knees inside your feet, butt down with back straight, hands out front with glove fingers pointing toward the ground, throwing hand up to an angle from glove with palm facing ball, head up.",
          "descriptionC": "Make sure fielders are getting into a good fielding position every time.",
          "descriptionD": "Have them work their glove from the ground up.",
          "descriptionE": "Grounders straight at fielder.",
          "descriptionF": "Moving them left to right.\nMoving them right to left.",
          "videoURL": "http://www.youtube.com/embed/WGL8JI5P_JA\\\nhttp://www.youtube.com/embed/Rrb2EiqYsfQ\\\n\nhttp://www.youtube.com/embed/hCsPelAiZI0\\"
        },
        {
          "title": "Glove Extension Slow Roller Drill",
          "descriptionA": "To the front side of your body- forehands",
          "descriptionB": "Field the ball low and with two hands",
          "descriptionC": "Add footwork to the ball",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Glove Side Groundballs",
          "descriptionA": "Work on 2 hands and getting low",
          "descriptionB": "Shift weight to where you field the ball",
          "descriptionC": "Exchange",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Barehand Grounders",
          "descriptionA": "Start by working the left right footwork and fielding the ball one handed.",
          "descriptionB": "Add second hand and steps after fielding.",
          "descriptionC": "Add a bucket or cone to have the player round off his approach to the fielding spot.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612406\\"
        },
        {
          "title": "Stationary Groundballs",
          "descriptionA": "Work on 2 hands and getting low",
          "descriptionB": "shift weight to where you field the ball",
          "descriptionC": "Exchange",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Groundball Footwork Drill",
          "descriptionA": "Get low and 2 hands",
          "descriptionB": "Footwork- right left down",
          "descriptionC": "Finish- right, left, throw to target",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Backhand And Forehand Grounders",
          "descriptionA": "This can be done hitting or just rolling grounders (or throwing) for more control. Concentrate on good fielding position.",
          "descriptionB": "Really important for fielders to bend their knees here to field ball in front of them.",
          "descriptionC": "First backhand should have both feet pointing forward fielding the ball backhanded.",
          "descriptionD": "Second back hand should incorporate crossover step, feet inline and backhand glove work.",
          "descriptionE": "End with some really hard backhands they really need to run for.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/Lo3jm9wTN38\\\nhttp://www.youtube.com/embed/_NqlwIAG834\\"
        },
        {
          "title": "Backhand With Crossover",
          "descriptionA": "Your foot will cross over your other and will do a lunge essentially",
          "descriptionB": "Have your mitt outside your leg and watch ball into mitt",
          "descriptionC": "Have a clear opening for the ball to enter the mitt",
          "descriptionD": "Get low",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Live Ground Balls",
          "descriptionA": "Advancing from the above stations, now the infielders are getting live ground balls. It is important here to keep the infielder moving through the grounder and not getting caught flat-footed.",
          "descriptionB": "As a right-hander approaching a ground ball, your last two steps are right foot-left foot, then get into fielding position to field the ball, continue on and step and throw to the base/target. It is opposite for lefties.",
          "descriptionC": "To reinforce, have fielders take a few extra steps toward target after throw.",
          "descriptionD": "Concentrate on fielding the ground ball in the middle of your body with your hands out front.",
          "descriptionE": "Two coaches hitting ground balls to two different positions (Coach 1 hitting ground balls to 3rd base/2nd base from left handed hitter's side. Coach 2 hitting ground balls to 1st base/shortstop from right handed hitter's side).",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "First Base Footwork",
          "descriptionA": "Practice going to the base and getting ready to recieve the throw",
          "descriptionB": "Rghty's- right foot on",
          "descriptionC": "Lefty's- left foot on",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "First Base Bad Throw Drill",
          "descriptionA": "Practice going to the base and getting ready to recieve the throw",
          "descriptionB": "Righty's- right foot on",
          "descriptionC": "Lefty's- left foot on",
          "descriptionD": "Add bad throws and extending to the ball",
          "descriptionE": "Front and backhands",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Big Hop Drill",
          "descriptionA": "Fielding a big hop is easy if it just pops up to you, but the trick is to have your athlete get the timing down of fielding and coming through the ball at about waist to chest high.",
          "descriptionB": "Never assume your going to get a easy bounce",
          "descriptionC": "Definitely practice on real grass or dirt because if you practice this inside you will always get the same hops but on the real field it's not as basic.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "In-Between Hops",
          "descriptionA": "Coach is throwing in-between hops to player to backhand, forehand and middle (can also be done during catch with players throwing to each other).",
          "descriptionB": "This is the hardest hop to catch and requires as much skill as luck, due to the uneven surface you may be playing on.",
          "descriptionC": "An in-between hop should be thrown so it bounces approximately 2'-4' in front of fielding position of the fielder. This is a hop he needs to catch on the way up from the bounce.",
          "descriptionD": "How to catch an in-between forehand hop - With fingers pointing to the ground on glove hand with arms extended and glove open and throwing hand close by with palm facing ball, place glove where you think ball will bounce up and funnel ball into stomach securing with throwing hand or push hand to ball, simultaneously close glove, and close wrist to chest. Either technique may have the same rate of success.",
          "descriptionE": "How to catch a backhand in-between hop - Same action as shorthand hop except now the height of your glove will be higher as the expected bounce will be higher. Down, out and up with the glove hand.",
          "descriptionF": "For balls right at you, either technique may work.\nConcentrate on good fielding position.",
          "videoURL": "http://www.youtube.com/embed/AkmTIDpNWZo\\\nhttp://www.youtube.com/embed/Pu0BK9QDu5w\\"
        },
        {
          "title": "Long Hops",
          "descriptionA": "Coach is throwing long hops to player to backhand, forehand and middle (can also be done during catch with players throwing to each other).",
          "descriptionB": "A long hop should be thrown approximately 12'-15' in front of fielding position of the fielder and should crest or be on the way down when fielded.",
          "descriptionC": "This hop is caught at its crest or on the way down. Keeping your eye on this hop is key here. A stab at the ball with your glove is acceptable as long as your glove is close to fielding position to react to the hop.",
          "descriptionD": "How to catch a long forehand hop - With fingers pointing to the ground or sideways on glove hand with arms extended and glove open and throwing hand close by with palm facing ball, place glove where you think ball will bounce up and funnel ball into stomach securing with throwing hand. This hop is played like a big hop forehand ground ball.",
          "descriptionE": "How to catch a long backhand hop - This assumes the hop will give you enough time to adjust with your glove. It is now okay to jab at it with your glove, because now you have time see the hop. Just make sure your glove starts in a close enough position to the hop in order to catch it. This hop is played like a big hop backhand ground ball.",
          "descriptionF": "For balls right at you, either technique may work.\nConcentrate on good fielding position.",
          "videoURL": "http://www.youtube.com/embed/UM-wx9UZTes\\\nhttp://www.youtube.com/embed/flLiA8FDwNU\\"
        },
        {
          "title": "Mix Em Up Grounders",
          "descriptionA": "Coach is hitting (throwing) grounders in all directions.",
          "descriptionB": "Make sure players get a rep of each variety.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/ervcjeJR8_0\\\nhttp://www.youtube.com/embed/GM7RqgsPmps\\"
        },
        {
          "title": "Knee Grounders",
          "descriptionA": "Players on knees (kneeling tall) about 15'-20' apart rolling grounders to each other.",
          "descriptionB": "Ball should be fielded out in front of body with glove fingers pointing to the ground.",
          "descriptionC": "Can also use this drill to throw short hops to each other.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612261\\"
        },
        {
          "title": "Knee Grounders Progression",
          "descriptionA": "Do the same as knee grounders",
          "descriptionB": "Add speed and types of hops",
          "descriptionC": "Front and backhands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Partner Hop Drill",
          "descriptionA": "Players about 30'-40' apart throwing each other short, medium, and long hops, backhand, forehand and middle.",
          "descriptionB": "It is important to start in an athletic fielding (ready) position here. Feet spread a little wider than shoulder width, knees inside feet, weight on the balls of the feet, slight bend in the waist, hands out front.",
          "descriptionC": "Partner should push the ball backhanded to his partner so the player gets a good bounce without too much speed.",
          "descriptionD": "Rollers can be added as well.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612278\\"
        },
        {
          "title": "Three Ball Charge",
          "descriptionA": "Place three balls on ground five feet apart.",
          "descriptionB": "Have player charge the balls, picking them up one at a time and firing them to first.",
          "descriptionC": "Add rollers once technique starts to shape up.",
          "descriptionD": "Encourages quick throws and arm slot",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612273\\"
        },
        {
          "title": "Force Outs And Double Plays",
          "descriptionA": "Coach hits grounders to infielders who make throw to 2B/SS for forceout at 2B.",
          "descriptionB": "In order to make this as game-like as possible, have player receiving throw start about 15' away from the bag near his normal position and come to the bag to make the play when coach hits the ball.",
          "descriptionC": "After force out is made, continue through the bag to get out of the way of the runner.",
          "descriptionD": "After force out is made and player is out of the way of the runner, practice getting ball out of glove quickly and getting into a good throwing position to look for the next play.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Phantom Ball Drill",
          "descriptionA": "Practice fielding ground balls without hitting ball.",
          "descriptionB": "Work feet and hands and everything you would usually do to field BUT without a baseball",
          "descriptionC": "Ball starts in players mit and emphasizes getting to the proper fielding position and proper footwork after fielding.",
          "descriptionD": "Practice gloveside, backhand and charging.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612269\\"
        },
        {
          "title": "Around The Cone Drill",
          "descriptionA": "Have fielder stand directly behind a cone (or bucket) about 12' away. Place a ball on the ground about 12' in front of the cone on the other side. Player starts in fielding ready position and comes around cone to the right (for right handers) and fields ball and throws to a target to his left.",
          "descriptionB": "As the fielder nears the ball, his last two steps (for right handers) is right foot/left foot, then field the ball with both hands.",
          "descriptionC": "Fielders throwing side foot should come in front of glove side foot and toward target during the throw.",
          "descriptionD": "To keep momentum going, take a couple extra steps after the throw towards target.",
          "descriptionE": "The fielding, throwing and extra steps after throw should all be done fluidly and in one single motion without stopping.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/6B47hkpCFL0\\"
        },
        {
          "title": "Around The Cone Drill Live",
          "descriptionA": "Have fielder stand directly behind a cone (or bucket) about 12' away. Player starts in fielding position and comes around cone to the right (for right hander) as coach rolls ball at him about 12' in front of the cone. Player fields ball and throws to a target to his left.",
          "descriptionB": "As the fielder nears the ball, his last two steps (for right handers) is right foot/left foot, then field the ball with both hands.",
          "descriptionC": "Fielders throwing side foot should come in front of glove side foot and toward target during the throw.",
          "descriptionD": "To keep momentum going, take a couple extra steps after the throw towards target.",
          "descriptionE": "The fielding, throwing and extra steps after throw should all be done fluidly and in one single motion without stopping.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/AZO8H8D-vRs\\"
        },
        {
          "title": "Diver Drill",
          "descriptionA": "Practice diving for balls starting from knees.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Knee Pop Ups",
          "descriptionA": "Practice catching pop ups while on knees.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612397\\"
        },
        {
          "title": "Clock Drill",
          "descriptionA": "Have your athlete move their mitt like a clock",
          "descriptionB": "Anything below the waist mitt- downward",
          "descriptionC": "Mitt up- upward throws",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Move Single Hand Catch",
          "descriptionA": "Use no mitt to catch",
          "descriptionB": "Works eye hand coordination",
          "descriptionC": "Soft hands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Five Fast",
          "descriptionA": "This is something you can use with the triangle drill.",
          "descriptionB": "Have them stand on there triangle of in good groundball poisition and have your athlete field 5 balls in a row very fast practicing receiving and exchange throughout the fielding process.",
          "descriptionC": "It focuses on quick hands and good form.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Three Man Flip Drill",
          "descriptionA": "Three players stand about 20' apart in a triangle shape. Player A rolls ball to player B, B underhand flips to player C. Player C rolls ball to Player A, A underhand flips to player B. Player B rolls ball to Player C, C underhand flips to player A. Repeat.",
          "descriptionB": "For older players, you can spread them out further and work on 2B/SS short throws or even further to work on ground balls/throws.",
          "descriptionC": "Switch directions.",
          "descriptionD": "When making flips, make sure flipping partner takes ball out of glove and shows it to partner before flipping and then takes an extra step after flip towards partner.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Slow Rollers",
          "descriptionA": "Line players up at 3rd base (or shortstop) and roll them slow rollers and have them throw to 1st base on the run.",
          "descriptionB": "Roll ball slow enough so player has to bare hand the ball.",
          "descriptionC": "Make sur. the right handed player has his left foot forward when fielding the ball",
          "descriptionD": "",
          "descriptionE": "Switch directions and have players throw from 1st base (or 2B) and throw to third base.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/-KvPLLvl5_c\\"
        },
        {
          "title": "Dribbler Drill",
          "descriptionA": "Set feet, scoop with both hands",
          "descriptionB": "If you play on a grass field, make sure you try this drill on grass as well to get used to planting on grass.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Fielding A Stationary Ball",
          "descriptionA": "A stationary ball in the field should almost always be picked up with the bare hand or throwing hand.",
          "descriptionB": "This is because the mitt is not as a reliable source when the ball is just sitting there! It often falls out. Then you have to do an exchange out of your mitt cleanly to make a throw, so by just picking it up bare hand you eliminate a step and have a better chance of picking it up the 1st time.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Side To Side Pick Ups",
          "descriptionA": "With coach about 10'-12' in front of player and kneeling down, roll one ball to players' left about 10' away.",
          "descriptionB": "Player has to shuffle sideways and make play and flip ball back to coach. As soon as coach receives ball he immediately rolls ball to players' right about 10' away.",
          "descriptionC": "Player shuffles sideways and makes play and flips ball back to coach. Repeat.",
          "descriptionD": "Can do forehand and backhand grounders.",
          "descriptionE": "No glove necessary.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/ilUbX3_njRU\\"
        },
        {
          "title": "Jump Pivot Drill",
          "descriptionA": "Working on footwork after properly fielded the ball",
          "descriptionB": "Jump or pivot to your target",
          "descriptionC": "Get low and balanced to make your throws",
          "descriptionD": "Make sure your feet are facing directly at your target after your pivot or hop",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612274"
        },
        {
          "title": "Wall Ball Drill",
          "descriptionA": "With the fielder facing the wall about 15' away and in ready position, coach throws the ball from behind the fielder off the wall. Fielder has to react and catch the ball.",
          "descriptionB": "Coach can throw left, right, one hoppers, line drives, fly balls, etc.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "No Legs",
          "descriptionA": "Have player kneel and get in a proper fielding position.",
          "descriptionB": "Start by rolling grounders and progress to underhanding liners and popups.",
          "descriptionC": "Older players can have the balls hit to them.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Pop Up In The Sun",
          "descriptionA": "Work with soft balls at first for safety",
          "descriptionB": "As the athlete gets more comfortable use hard balls",
          "descriptionC": "Use the mitt to block the sun from your eyes and travel to the ball",
          "descriptionD": "Sunglasses help as well",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Charging Race",
          "descriptionA": "Have two or three players start the drill facing away from the coach.",
          "descriptionB": "When coach says go the players turn and charge at the coach who rolls or hits ball.",
          "descriptionC": "Players racr to field ball while staying on their feet.",
          "descriptionD": "A second ball can be rolled",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        }
      ]
    },
    {
      "id": "5c592f9b521d8d30aa816790",
      "title": "Outfield Stations",
      "items": [
        {
          "title": "Fielding A Ball As An Outfielder",
          "descriptionA": "Always use a creep step",
          "descriptionB": "Use two hands when catching",
          "descriptionC": "Grounders to outfield- keep ball in front",
          "descriptionD": "Communicate with other fielders",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/232832751"
        },
        {
          "title": "Wide Receiver Drill",
          "descriptionA": "Have players running left/right and catching fly balls.",
          "descriptionB": "Player runs to right and coach throws ball. Outfielder has to catch ball.",
          "descriptionC": "Player runs to left and coach throws ball. Outfielder has to catch ball.",
          "descriptionD": "Variation: Coach throws ball back left/right. After ball is caught, crow hop and quickly throw ball in.",
          "descriptionE": "Variation: Coach throws ball in left/right. Catch ball above head. After ball is caught, crow hop and quickly throw ball in.",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612405"
        },
        {
          "title": "Outfield Diving",
          "descriptionA": "Utilize head first dives vs. slides",
          "descriptionB": "Practice popping up after you make the play",
          "descriptionC": "Make sure you don't land on your hand sor wrist to avoid injury",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612426"
        },
        {
          "title": "Outfield Sliding Catches",
          "descriptionA": "Have to have a partner throwing the balls",
          "descriptionB": "To slide- you put one leg straight out and the other bent",
          "descriptionC": "Pop up slide- will help as you get comfortable",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612425"
        },
        {
          "title": "Anticipating A Bad Throw",
          "descriptionA": "Always be facing the thrower",
          "descriptionB": "Have your feet moving toward the ball",
          "descriptionC": "Be ready to get in front of ball and block",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Wiffle Ball Catch",
          "descriptionA": "Great for beginners",
          "descriptionB": "Start underhand so they can learn to catch",
          "descriptionC": "Work up to throwing overhand",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Self Toss Crow Hop Drill",
          "descriptionA": "Get one foot behind the other",
          "descriptionB": "Throw ball up to yourself",
          "descriptionC": "Get behind the ball and catch cleanly with 2 hands",
          "descriptionD": "Rotate body sideways as you exchange the ball to throwing hand for throw",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612399"
        },
        {
          "title": "Star Drill",
          "descriptionA": "Need multiple baseballs",
          "descriptionB": "Have player run in star pattern as they go forward, back, left right, to catch ball",
          "descriptionC": "Thrower- makes them harder or easier",
          "descriptionD": "Make them have to move their feet to catch the ball",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180599380"
        },
        {
          "title": "Ball Passes You In The Outfield",
          "descriptionA": "Don't feel sorry for yourself and put your head down",
          "descriptionB": "immediately run after the ball and get it to a fielder",
          "descriptionC": "Pick ball up with bare hand",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612430"
        },
        {
          "title": "Fielding Ball By The Fence",
          "descriptionA": "Pick up ball with bare hand so you don't need to exchange out of mitt",
          "descriptionB": "Bend down and use legs to gain momentum and make throw to fielders",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612429"
        },
        {
          "title": "Line Drives",
          "descriptionA": "Coach is throwing line drives directly at, to the left or to the right of the fielders.",
          "descriptionB": "Make sure player is in proper ready position. Athletic stance, hands out front, elbows down and relaxed.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/vGCtrE5v8Ws\\"
        },
        {
          "title": "Pop Ups",
          "descriptionA": "Coach is hitting (or throwing) pop ups directly at, to the left or to the right of the fielders (for more control, tennis racket is great).",
          "descriptionB": "Make sure players are catching ball above their head (in front of their face) with two hands.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/VqDDRFRZgCk\\"
        },
        {
          "title": "Back Left",
          "descriptionA": "Working on going back left to receive ball.",
          "descriptionB": "Fielders first step is a jab step, then hip turn and go.",
          "descriptionC": "After ball is caught, turn, crow hop and quickly throw ball in.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/wCjy4eQsEN0\\"
        },
        {
          "title": "Back Right",
          "descriptionA": "Working on going back right to receive ball.",
          "descriptionB": "Working on coming in to the left to receive ball.",
          "descriptionC": "Catch ball above head in strong throwing position.",
          "descriptionD": "Fielders first step is a jab step, then hip turn and go.",
          "descriptionE": "After ball is caught, turn, crow hop and quickly throw ball in.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/wpttQ-9z8CA\\"
        },
        {
          "title": "Breaking In Left",
          "descriptionA": "Working on coming in to the left to receive ball.",
          "descriptionB": "Catch ball above head in strong throwing position.",
          "descriptionC": "Crow hop and quickly throw ball in.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Breaking In Right",
          "descriptionA": "Working on coming in to the right to receive ball.",
          "descriptionB": "Catch ball above head in strong throwing position.",
          "descriptionC": "Crow hop and quickly throw ball in.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Straight Back",
          "descriptionA": "Working on going straight back to receive ball.",
          "descriptionB": "Fielders first step is a jab step, then hip turn and go.",
          "descriptionC": "For ball straight at you, it is a quicker return throw to turn to your throwing hand side to retrieve ball.",
          "descriptionD": "For a right hander, it is a jab step with left foot, then hip turn to right and go.",
          "descriptionE": "After ball is caught, crow hop and quickly throw ball in.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/3ZN9f_50a_c\\"
        },
        {
          "title": "Mix Em Up Fly Balls",
          "descriptionA": "Coach hitting (throwing) fly balls to all directions.",
          "descriptionB": "After ball is caught, crow hop and quickly throw ball in.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Outfield Ground Balls",
          "descriptionA": "Make sure you keep the ball in front of you.",
          "descriptionB": "On a hard hit grounder, going down to one knee is advised.",
          "descriptionC": "Play the grounder like an infielder. Field ball in middle of body, hands out front.",
          "descriptionD": "When charging a ball, execute footwork to field ball in front of toes on front foot allowing balance to get good throwing momentum.",
          "descriptionE": "After ball is caught, crow hop and quickly throw ball in.",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Preventing Extra Bases",
          "descriptionA": "Hit grounders to left/right of outfielder.",
          "descriptionB": "Take proper angle to cut ball off.",
          "descriptionC": "Get in front of ball.",
          "descriptionD": "After ball is caught, crow hop and quickly throw ball in.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Self Toss And Crow Hop",
          "descriptionA": "Throw ball up to yourself and work towards the ball",
          "descriptionB": "catch the ball between your head and chest while moving your feet",
          "descriptionC": "Catch ball, shuffle your feet right, left, and throw to a target",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Bloop Flyballs",
          "descriptionA": "Aggressively closing on bloopers, calling off infielder",
          "descriptionB": "Stress running through the ball.",
          "descriptionC": "Point out weak balls you can be aggressive with because they will not get too far from you and liners that could get pretty far past you that need to be knocked down..",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Tennis Ball Triangle",
          "descriptionA": "SS, 2B CF all at their positions. With a tennis racket, hit a pop up between the 3 fielders. One has to call and catch it.",
          "descriptionB": "Work on communication between the fielders. Have catcher call ball 3 times.",
          "descriptionC": "Infielder closest to fly ball can help back up outfielder. Infielder farthest away from fly ball must go and cover 2B.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Me You Flyball Lines",
          "descriptionA": "Set two players up about 30' apart. Coach throws the ball in between them and one fielder has to call/catch the ball.",
          "descriptionB": "Have catcher call ball 3 times.",
          "descriptionC": "Other outfielder backs up catch.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612429\\"
        },
        {
          "title": "Ball To The Wall",
          "descriptionA": "Roll ball to outfield wall.",
          "descriptionB": "Outfielder chases ball, plants feet, grabs ball, shuffles and throws.",
          "descriptionC": "Add second ball to stress importance of footwork providing some momentum.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "500",
          "descriptionA": "Split the team into 2 groups and have fielders in right field/left field and one hitter for each group. A game where hitter throws up the ball and hits it to the fielders.",
          "descriptionB": "If you catch it on the fly, 100 points. One bounce, 75 points. Two bounces, 50 points. Three or more, 25 points.",
          "descriptionC": "Keep track of your score. If you miss the catch, deduct that amount from your score.",
          "descriptionD": "Make sure kids are calling for the ball and communicating.",
          "descriptionE": "First one to 500 gets to hit.",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Blind Pop Ups",
          "descriptionA": "Have fielders about 20'-30' in front of coach with their back to you. Throw or hit a fly ball and yell \\\"ball.\\\"",
          "descriptionB": "When fielders hear \\\"ball,\\\" they have to turn around, find the ball and catch it.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Outfield Target Practice",
          "descriptionA": "Pick a spot on the backstop to hit or a bucket on home plate.",
          "descriptionB": "Emphasis line drive throws.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Fielding Ball By Outfield Wall",
          "descriptionA": "Outfielder chases ball, plants feet, grabs ball, shuffles and throws.",
          "descriptionB": "Grab with barehand for added quickness",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Chasing A Ball Drill",
          "descriptionA": "Have someone throw the ball and make the fielder keep in front",
          "descriptionB": "Practice range to the ball by taking wider angles",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Outfield Sliding Drill",
          "descriptionA": "Throw the ball up and have the player dive as they catch",
          "descriptionB": "Work different directions overhead, left, right, and in front",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Outfield Diving Drill",
          "descriptionA": "Start on knees and have player practice extending there arm for ball to make catch and landing on chest",
          "descriptionB": "Then add diving from your feet",
          "descriptionC": "Make sure its nice soft grass or that they are wearing pants so they don't injure themselves",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Robbing A Home Run Drill",
          "descriptionA": "Position yourself accordingly and get low and jump to catch",
          "descriptionB": "Position yourself accordingly and get low and jump to catch",
          "descriptionC": "Try to time the ball so you get a better jump",
          "descriptionD": "Use the fence to help boost you higher to make the catch",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Do Or Die Ground Balls",
          "descriptionA": "Have outfielders in center field and hit them grounders. They have to throw the runner out at home plate.",
          "descriptionB": "Have outfielder field ball on the run on their glove side, take a crow hop and make a strong throw through the cutoff man.",
          "descriptionC": "Remind player of proper four seam grip.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/TdaFKzk9aZk\\"
        },
        {
          "title": "Do Or Die Fly Balls",
          "descriptionA": "Have outfielders in center field and hit them fly balls. They have to throw the runner out at home plate.",
          "descriptionB": "Outfielders have to get themselves into position behind the ball, create some momentum to their target before the catch, catch the ball above their head, take a crow hop and make a strong throw through the cutoff man.",
          "descriptionC": "Remind player of proper four seam grip.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Live Ground Balls",
          "descriptionA": "Advancing from the above stations, now the infielders are getting live ground balls. It is important here to keep the infielder moving through the grounder and not getting caught flat-footed.",
          "descriptionB": "As a right-hander approaching a ground ball, your last two steps are right foot-left foot, then get into fielding position to field the ball, continue on and step and throw to the base/target. It is opposite for lefties.",
          "descriptionC": "To reinforce, have fielders take a few extra steps toward target after throw.",
          "descriptionD": "Concentrate on fielding the ground ball in the middle of your body with your hands out front.",
          "descriptionE": "Two coaches hitting ground balls to two different positions (Coach 1 hitting ground balls to 3rd base/2nd base from left handed hitter's side. Coach 2 hitting ground balls to 1st base/shortstop from right handed hitter's side).",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "First Base Footwork",
          "descriptionA": "Practice going to the base and getting ready to recieve the throw",
          "descriptionB": "Rghty's- right foot on",
          "descriptionC": "Lefty's- left foot on",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "First Base Bad Throw Drill",
          "descriptionA": "Practice going to the base and getting ready to recieve the throw",
          "descriptionB": "Righty's- right foot on",
          "descriptionC": "Lefty's- left foot on",
          "descriptionD": "Add bad throws and extending to the ball",
          "descriptionE": "Front and backhands",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Big Hop Drill",
          "descriptionA": "Fielding a big hop is easy if it just pops up to you, but the trick is to have your athlete get the timing down of fielding and coming through the ball at about waist to chest high.",
          "descriptionB": "Never assume your going to get a easy bounce",
          "descriptionC": "Definitely practice on real grass or dirt because if you practice this inside you will always get the same hops but on the real field it's not as basic.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "In-Between Hops",
          "descriptionA": "Coach is throwing in-between hops to player to backhand, forehand and middle (can also be done during catch with players throwing to each other).",
          "descriptionB": "This is the hardest hop to catch and requires as much skill as luck, due to the uneven surface you may be playing on.",
          "descriptionC": "An in-between hop should be thrown so it bounces approximately 2'-4' in front of fielding position of the fielder. This is a hop he needs to catch on the way up from the bounce.",
          "descriptionD": "How to catch an in-between forehand hop - With fingers pointing to the ground on glove hand with arms extended and glove open and throwing hand close by with palm facing ball, place glove where you think ball will bounce up and funnel ball into stomach securing with throwing hand or push hand to ball, simultaneously close glove, and close wrist to chest. Either technique may have the same rate of success.",
          "descriptionE": "How to catch a backhand in-between hop - Same action as shorthand hop except now the height of your glove will be higher as the expected bounce will be higher. Down, out and up with the glove hand.",
          "descriptionF": "For balls right at you, either technique may work.\nConcentrate on good fielding position.",
          "videoURL": "http://www.youtube.com/embed/AkmTIDpNWZo",
          "videoURL2": "http://www.youtube.com/embed/Pu0BK9QDu5w"
        },
        {
          "title": "Long Hops",
          "descriptionA": "Coach is throwing long hops to player to backhand, forehand and middle (can also be done during catch with players throwing to each other).",
          "descriptionB": "A long hop should be thrown approximately 12'-15' in front of fielding position of the fielder and should crest or be on the way down when fielded.",
          "descriptionC": "This hop is caught at its crest or on the way down. Keeping your eye on this hop is key here. A stab at the ball with your glove is acceptable as long as your glove is close to fielding position to react to the hop.",
          "descriptionD": "How to catch a long forehand hop - With fingers pointing to the ground or sideways on glove hand with arms extended and glove open and throwing hand close by with palm facing ball, place glove where you think ball will bounce up and funnel ball into stomach securing with throwing hand. This hop is played like a big hop forehand ground ball.",
          "descriptionE": "How to catch a long backhand hop - This assumes the hop will give you enough time to adjust with your glove. It is now okay to jab at it with your glove, because now you have time see the hop. Just make sure your glove starts in a close enough position to the hop in order to catch it. This hop is played like a big hop backhand ground ball.",
          "descriptionF": "For balls right at you, either technique may work.\nConcentrate on good fielding position.",
          "videoURL": "http://www.youtube.com/embed/UM-wx9UZTes",
          "videoURL2": "http://www.youtube.com/embed/flLiA8FDwNU"
        },
        {
          "title": "Mix Em Up Grounders",
          "descriptionA": "Coach is hitting (throwing) grounders in all directions.",
          "descriptionB": "Make sure players get a rep of each variety.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/ervcjeJR8_0",
          "videourl2": "http://www.youtube.com/embed/GM7RqgsPmps"
        },
        {
          "title": "Knee Grounders",
          "descriptionA": "Players on knees (kneeling tall) about 15'-20' apart rolling grounders to each other.",
          "descriptionB": "Ball should be fielded out in front of body with glove fingers pointing to the ground.",
          "descriptionC": "Can also use this drill to throw short hops to each other.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612261"
        },
        {
          "title": "Knee Grounders Progression",
          "descriptionA": "Do the same as knee grounders",
          "descriptionB": "Add speed and types of hops",
          "descriptionC": "Front and backhands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Partner Hop Drill",
          "descriptionA": "Players about 30'-40' apart throwing each other short, medium, and long hops, backhand, forehand and middle.",
          "descriptionB": "It is important to start in an athletic fielding (ready) position here. Feet spread a little wider than shoulder width, knees inside feet, weight on the balls of the feet, slight bend in the waist, hands out front.",
          "descriptionC": "Partner should push the ball backhanded to his partner so the player gets a good bounce without too much speed.",
          "descriptionD": "Rollers can be added as well.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612278"
        },
        {
          "title": "Three Ball Charge",
          "descriptionA": "Place three balls on ground five feet apart.",
          "descriptionB": "Have player charge the balls, picking them up one at a time and firing them to first.",
          "descriptionC": "Add rollers once technique starts to shape up.",
          "descriptionD": "Encourages quick throws and arm slot",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612273"
        },
        {
          "title": "Force Outs And Double Plays",
          "descriptionA": "Coach hits grounders to infielders who make throw to 2B/SS for forceout at 2B.",
          "descriptionB": "In order to make this as game-like as possible, have player receiving throw start about 15' away from the bag near his normal position and come to the bag to make the play when coach hits the ball.",
          "descriptionC": "After force out is made, continue through the bag to get out of the way of the runner.",
          "descriptionD": "After force out is made and player is out of the way of the runner, practice getting ball out of glove quickly and getting into a good throwing position to look for the next play.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Phantom Ball Drill",
          "descriptionA": "Practice fielding ground balls without hitting ball.",
          "descriptionB": "Work feet and hands and everything you would usually do to field BUT without a baseball",
          "descriptionC": "Ball starts in players mit and emphasizes getting to the proper fielding position and proper footwork after fielding.",
          "descriptionD": "Practice gloveside, backhand and charging.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612269"
        },
        {
          "title": "Around The Cone Drill",
          "descriptionA": "Have fielder stand directly behind a cone (or bucket) about 12' away. Place a ball on the ground about 12' in front of the cone on the other side. Player starts in fielding ready position and comes around cone to the right (for right handers) and fields ball and throws to a target to his left.",
          "descriptionB": "As the fielder nears the ball, his last two steps (for right handers) is right foot/left foot, then field the ball with both hands.",
          "descriptionC": "Fielders throwing side foot should come in front of glove side foot and toward target during the throw.",
          "descriptionD": "To keep momentum going, take a couple extra steps after the throw towards target.",
          "descriptionE": "The fielding, throwing and extra steps after throw should all be done fluidly and in one single motion without stopping.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/6B47hkpCFL0"
        },
        {
          "title": "Around The Cone Drill Live",
          "descriptionA": "Have fielder stand directly behind a cone (or bucket) about 12' away. Player starts in fielding position and comes around cone to the right (for right hander) as coach rolls ball at him about 12' in front of the cone. Player fields ball and throws to a target to his left.",
          "descriptionB": "As the fielder nears the ball, his last two steps (for right handers) is right foot/left foot, then field the ball with both hands.",
          "descriptionC": "Fielders throwing side foot should come in front of glove side foot and toward target during the throw.",
          "descriptionD": "To keep momentum going, take a couple extra steps after the throw towards target.",
          "descriptionE": "The fielding, throwing and extra steps after throw should all be done fluidly and in one single motion without stopping.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/AZO8H8D-vRs"
        },
        {
          "title": "Diver Drill",
          "descriptionA": "Practice diving for balls starting from knees.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Knee Pop Ups",
          "descriptionA": "Practice catching pop ups while on knees.",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612397"
        },
        {
          "title": "Clock Drill",
          "descriptionA": "Have your athlete move their mitt like a clock",
          "descriptionB": "Anything below the waist mitt- downward",
          "descriptionC": "Mitt up- upward throws",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Move Single Hand Catch",
          "descriptionA": "Use no mitt to catch",
          "descriptionB": "Works eye hand coordination",
          "descriptionC": "Soft hands",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Five Fast",
          "descriptionA": "This is something you can use with the triangle drill.",
          "descriptionB": "Have them stand on there triangle of in good groundball poisition and have your athlete field 5 balls in a row very fast practicing receiving and exchange throughout the fielding process.",
          "descriptionC": "It focuses on quick hands and good form.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Three Man Flip Drill",
          "descriptionA": "Three players stand about 20' apart in a triangle shape. Player A rolls ball to player B, B underhand flips to player C. Player C rolls ball to Player A, A underhand flips to player B. Player B rolls ball to Player C, C underhand flips to player A. Repeat.",
          "descriptionB": "For older players, you can spread them out further and work on 2B/SS short throws or even further to work on ground balls/throws.",
          "descriptionC": "Switch directions.",
          "descriptionD": "When making flips, make sure flipping partner takes ball out of glove and shows it to partner before flipping and then takes an extra step after flip towards partner.",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Slow Rollers",
          "descriptionA": "Line players up at 3rd base (or shortstop) and roll them slow rollers and have them throw to 1st base on the run.",
          "descriptionB": "Roll ball slow enough so player has to bare hand the ball.",
          "descriptionC": "Make sur. the right handed player has his left foot forward when fielding the ball",
          "descriptionD": "",
          "descriptionE": "Switch directions and have players throw from 1st base (or 2B) and throw to third base.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/-KvPLLvl5_c\\"
        },
        {
          "title": "Dribbler Drill",
          "descriptionA": "Set feet, scoop with both hands",
          "descriptionB": "If you play on a grass field, make sure you try this drill on grass as well to get used to planting on grass.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Fielding A Stationary Ball",
          "descriptionA": "A stationary ball in the field should almost always be picked up with the bare hand or throwing hand.",
          "descriptionB": "This is because the mitt is not as a reliable source when the ball is just sitting there! It often falls out. Then you have to do an exchange out of your mitt cleanly to make a throw, so by just picking it up bare hand you eliminate a step and have a better chance of picking it up the 1st time.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Side To Side Pick Ups",
          "descriptionA": "With coach about 10'-12' in front of player and kneeling down, roll one ball to players' left about 10' away.",
          "descriptionB": "Player has to shuffle sideways and make play and flip ball back to coach. As soon as coach receives ball he immediately rolls ball to players' right about 10' away.",
          "descriptionC": "Player shuffles sideways and makes play and flips ball back to coach. Repeat.",
          "descriptionD": "Can do forehand and backhand grounders.",
          "descriptionE": "No glove necessary.",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/ilUbX3_njRU\\"
        },
        {
          "title": "Jump Pivot Drill",
          "descriptionA": "Working on footwork after properly fielded the ball",
          "descriptionB": "Jump or pivot to your target",
          "descriptionC": "Get low and balanced to make your throws",
          "descriptionD": "Make sure your feet are facing directly at your target after your pivot or hop",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "https://player.vimeo.com/video/180612274"
        },
        {
          "title": "Wall Ball Drill",
          "descriptionA": "With the fielder facing the wall about 15' away and in ready position, coach throws the ball from behind the fielder off the wall. Fielder has to react and catch the ball.",
          "descriptionB": "Coach can throw left, right, one hoppers, line drives, fly balls, etc.",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "No Legs",
          "descriptionA": "Have player kneel and get in a proper fielding position.",
          "descriptionB": "Start by rolling grounders and progress to underhanding liners and popups.",
          "descriptionC": "Older players can have the balls hit to them.",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Pop Up In The Sun",
          "descriptionA": "Work with soft balls at first for safety",
          "descriptionB": "As the athlete gets more comfortable use hard balls",
          "descriptionC": "Use the mitt to block the sun from your eyes and travel to the ball",
          "descriptionD": "Sunglasses help as well",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        },
        {
          "title": "Charging Race",
          "descriptionA": "Have two or three players start the drill facing away from the coach.",
          "descriptionB": "When coach says go the players turn and charge at the coach who rolls or hits ball.",
          "descriptionC": "Players racr to field ball while staying on their feet.",
          "descriptionD": "A second ball can be rolled",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": ""
        }
      ]
    },
    {
      "id": "5c59300a521d8d30aa816792",
      "title": "Drills With Practice Aid",
      "items": [
        {
          "title": "Grounders With Pancake Mitt",
          "descriptionA": "Concentrates on not gloving ball on ground balls",
          "descriptionB": "",
          "descriptonC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/qVrGaPZJzt4\\"
        },
        {
          "title": "Quick Hands With Pancake Mitt",
          "descriptionA": "Concentrates on not gloving ball on throws",
          "descriptionB": "",
          "descriptonC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/Nz9d0qf2M2k\\"
        },
        {
          "title": "Soft Toss With Insider Bat",
          "descriptionA": "Promotes promotes proper grip, hand placement and proper swing path before, during and after contact is made",
          "descriptionB": "",
          "descriptonC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/6Ql5VMsf5Po\\"
        },
        {
          "title": "Front Toss With Hit Stick Golf Wiffles",
          "descriptionA": "Promotes concentration and solid contact",
          "descriptionB": "",
          "descriptonC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/tUFq7l7UkME\\"
        }
      ]
    },
    {
      "id": "5c593038521d8d30aa816793",
      "title": "Drills With Practice Aid .",
      "items": [
        {
          "title": "Grounders With Pancake Mitt",
          "descriptionA": "Concentrates on not gloving ball on ground balls",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/qVrGaPZJzt4\\",
          "id": "YYYY02DD10026902"
        },
        {
          "title": "Soft Toss With Insider Bat",
          "descriptionA": "Promotes promotes proper grip, hand placement and proper swing path before, during and after contact is made",
          "descriptionB": "",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/6Ql5VMsf5Po\\",
          "id": "2019020510021902"
        }
      ]
    },
    {
      "id": "5c5935e1d6cdbd59339c7690",
      "title": "Test Category",
      "items": [
        {
          "title": "Grounders With Pancake Mitt test",
          "descriptionA": "Concentrates on not gloving ball on ground balls",
          "descriptionB": "Concentrates on not gloving ball on ground balls",
          "descriptionC": "",
          "descriptionD": "",
          "descriptionE": "",
          "descriptionF": "",
          "videoURL": "http://www.youtube.com/embed/qVrGaPZJzt4\\",
          "id": "2019020511028002"
        }
      ]
    }
  ];

  public testData: any;
  ngOnInit () {
    this.testData = this.Data;
  }


}
