import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

const headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class PracticePlannerService {

  constructor(private http: HttpClient) { }

  getRequest(url) {
    return this.http.get(`${url}`, { headers: headers });
  }

  postRequest(url, data) {
    return this.http.post(`${url}`, data, { headers: headers });
  }

  putRequest(url, data) {
    return this.http.put(`${url}`, data, { headers: headers });
  }

  patchRequest(url, data) {
    return this.http.patch(`${url}`, data, { headers: headers });
  }

  removeRequest(url) {
    return this.http.delete(`${url}`,  { headers: headers });
  }

}
