import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'ng2-dropdown';
import { PracticePlannerService } from './practice-planner.service';

import { DrillsComponent } from './drills/drills.component';
import { DrillsDetailsComponent } from './drills-details/drills-details.component';

@NgModule({
  declarations: [DrillsComponent, DrillsDetailsComponent],
  imports: [
    CommonModule,
    NgxSmartModalModule.forRoot(),
    FormsModule,
    DropdownModule
  ],
  providers: [
    DatePipe,
    PracticePlannerService
  ],
  exports: [
    DrillsComponent,
    DrillsDetailsComponent
  ]
})
export class DrillsModule { }
