import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrillsDetailsComponent } from './drills-details.component';

describe('DrillsDetailsComponent', () => {
  let component: DrillsDetailsComponent;
  let fixture: ComponentFixture<DrillsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrillsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrillsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
