import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PracticePlannerService } from '../practice-planner.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-drills-details',
  templateUrl: './drills-details.component.html',
  styleUrls: ['./drills-details.component.css']
})
export class DrillsDetailsComponent implements OnInit {

  @Input() data: any;
  public drill: any;
  public plans: any;
  public selectedDrill: any;
  public selectedIndex: number;

  constructor(
    private practiceService: PracticePlannerService,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    public sanitizer: DomSanitizer,
    public ngxSmartModalService: NgxSmartModalService
  ) { }

  ngOnInit() {
    let drillIndex;
    this.route.queryParams
      .filter(params => params.id)
      .subscribe(params => {
        drillIndex = params.id;
      });
    this.practiceService.getRequest(`api/practicePlans/${drillIndex}`).subscribe(
      (res: any) => {
        this.drill = res;
      },
      error1 => {
        console.log('errr', error1);
      }
    );

     this.practiceService.getRequest('api/saveplans').subscribe(
       (res: any) => {
         this.plans = res;
       },
       error1 => {
         console.log('error', error1);
       }
     );

  }

  back(): void {
    this.router.navigate(['drills']);
  }

  remove(index): void {
    this.ngxSmartModalService.getModal('removeModal').open();
    this.selectedIndex = index;
  }

  submitRemove(): void {
    this.drill.items.splice(this.selectedIndex, 1);
    this.practiceService.putRequest(`api/practicePlans/${this.drill.id}`, this.drill).subscribe(
      (res: any) => {
        this.ngxSmartModalService.getModal('removeModal').close();
        this.selectedIndex = NaN;
      },
      error1 => {
        console.log('remove error', error1);
      }
    );
  }

  edit(drill, i): void {
    this.selectedDrill = drill;
    this.selectedIndex = i;
    this.ngxSmartModalService.getModal('editModal').open();
  }

  submitEdit(): void {
    if ( this.selectedIndex || !isNaN(this.selectedIndex)) {
      this.drill.items[this.selectedIndex] = this.selectedDrill;
    } else {
      this.drill.items.push(this.selectedDrill);
    }
    this.practiceService.putRequest(`api/practicePlans/${this.drill.id}`, this.drill).subscribe(
      (res: any) => {
        this.ngxSmartModalService.getModal('editModal').close();
        this.selectedIndex = NaN;
      },
      error1 => {
        console.error('edit error', error1);
      }
    );
  }

  create(): void {
    this.selectedDrill = {
      title: '',
      descriptionA: '',
      descriptionB: '',
      descriptionC: '',
      descriptionD: '',
      descriptionE: '',
      descriptionF: '',
      videoURL: '',
      id: this.datePipe.transform(new Date(),'yyyy MM dd HH MM SS MM').replace(/\s/g,'')
    };
    this.ngxSmartModalService.getModal('editModal').open();
  }

  check(plan, drill): boolean {
    let checked = plan.description[0].find( (item) => {
      return item.id === drill.id;
    } );
    return checked ? true : false;
  }

  addToPlan(plan, drill) {
    let reomveIndex;
    let checked = plan.description[0].find( (item, index) => {
        if ( item.id === drill.id) {
          reomveIndex = index;
        }
      return item.id === drill.id;
    });
    if ( checked ) {
      plan.description[0].splice(reomveIndex, 1);
      console.log('remove', plan);
    } else {
      plan.description[0].push(drill);
      console.log('add', plan);
    }
      this.practiceService.putRequest(`api/saveplans/${plan.id}`, plan).subscribe(
        (res: any) => {
          console.log('added', res);
        },
        error1 => {
          console.error('err', error1);
        }
      );
    }
}
