import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PracticePlannerService } from '../practice-planner.service';

@Component({
  selector: 'app-drills',
  templateUrl: './drills.component.html',
  styleUrls: ['./drills.component.css']
})
export class DrillsComponent implements OnInit {

  @Input() data: any;
  public categoryTitle: string;
  public selectedIndex: number;
  public actionType: string;

  constructor(
    // private localService: LocalService,
    private practiceService: PracticePlannerService,
    private router: Router,
    public ngxSmartModalService: NgxSmartModalService,
    ) { }

  ngOnInit() {
    console.log('-----', this.data);
  }

  detailClicked(drill, index): void {
    this.router.navigate(['drills/detail'], {queryParams: {id: drill.id, i: index }});
  }

  createCategoty(): void {
    this.categoryTitle = '';
    this.actionType = 'submitCreate';
    this.ngxSmartModalService.getModal('createModal').open();
  }

  edit(category, i): void {
    this.selectedIndex = i;
    this.actionType = 'submitEdit';
    this.categoryTitle = category.title;
    this.ngxSmartModalService.getModal('createModal').open();
  }

  submitEdit(data): void {
    this.practiceService.patchRequest(`api/practicePlans/${this.data[this.selectedIndex].id}`, data).subscribe(
      (res: any) => {
        this.data[this.selectedIndex] = res;
        this.categoryTitle = '';
        this.ngxSmartModalService.getModal('createModal').close();
      },
      error1 => {
        console.error('err', error1);
      }
    );
  }

  submitCreate (data): void {
    data['items'] = [];
    this.practiceService.postRequest('api/practicePlans', data).subscribe(
      (res: any) => {
        console.log('res', res);
        this.data.push(res);
        this.categoryTitle = '';
        this.ngxSmartModalService.getModal('createModal').close();
      },
      error1 => {
        console.error('err', error1);
      }
    );
  }

  submit(): void {
    console.log('title', this.categoryTitle);
    let data = {
      title: this.categoryTitle
    };
    this[this.actionType](data);
  }

  remove(i): void {
    this.selectedIndex = i;
    this.ngxSmartModalService.getModal('removeModal').open();
  }

  submitRemove(): void {
    this.practiceService.removeRequest(`api/practicePlans/${this.data[this.selectedIndex].id}`).subscribe(
      (res: any) => {
        this.data.splice(this.selectedIndex, 1);
        this.ngxSmartModalService.getModal('removeModal').close();
        this.selectedIndex = NaN;
      },
      error1 => {
        console.error('err', error1);
      }
    );
  }

}
