import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DrillsComponent} from './drills/drills.component';
import {DrillsDetailsComponent} from './drills-details/drills-details.component';

const routes: Routes = [
  {path: '', component: DrillsComponent},
  {path: 'detail', component: DrillsDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DrillsRoutingModule { }
