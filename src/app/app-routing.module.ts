import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {DrillsDetailsComponent} from './drills/drills-details/drills-details.component';
import {DrillsComponent} from './drills/drills/drills.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
  {path: '', component: TestComponent, pathMatch: 'full'},
  {path: 'drills/detail', component: DrillsDetailsComponent},
  // {path: 'lineup', loadChildren: './lineup/lineup.module#LineupModule'},
  // {path: 'plans', loadChildren: './plans/plans.module#PlansModule'},
  // {path: 'drills', loadChildren: './drills/drills.module#DrillsModule'},
  // {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
